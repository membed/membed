# MEmbed Database (membed.db)
The database 'membed.db' is an ASE database in which the information of the embedding calculation is stored.
The different types of entries and their fields (which are not ASE standard fields) are described further below.

Note that during optimizations, the database is deleted and newly written after each optimization step, therefore, only the database from the last optimization step is preserved.

## Periodic crystal structure

| Database field                | Description                                                                          |
|:----------------------------- |:------------------------------------------------------------------------------------ |
| name                          | Structure name. Always named 'crystal'.                                              |
| nmolecules                    | Number of molecules in the unit cell                                                 |
| low_energy                    | Energy of the low-level periodic calculation in eV                                   |
| high_energy                   | Through embedding approximated high-level periodic energy in eV                      |
| data.low_forces               | Forces of the low-level periodic calculation in eV/A                                 |
| data.high_forces              | Through embedding approximated high-level forces in eV/A                             |
| data.high_forces_sym          | Symmetrized high-level forces                                                        |
| data.low_stress               | Stress tensor of the low-level periodic calculation in eV/A**3                       |
| data.high_stress              | Through embedding approximated high-level stress tensor in eV/A**3                   |
| data.high_stress_sym          | Symmetrized high-level stress tensor                                                 |
| data.settings                 | Dictionary containing all settings                                                   |
| data.sc_dimensions            | Dimensions of the supercell generated in order to identify multimers                 |
| data.high_force_sets          | Collection of the approximated high-level forces of the displaced supercells         |
| data.high_force_sets_lines    | Lines of the FORCE_SETS file interpretable by phonopy                                |
| data.mesh_dict_gamma          | Dictionary obtained from get_mesh_dict() at the gamma point (see Phonopy Python API) |
| data.thermal_properties_gamma | Dictionary containing thermal properties at the gamma point (see Phonopy Python API) |
| data.thermal_properties       | Dictionary containg thermal properties using the q-grid given by vib_q_grid          |

## Monomers
| Database field       | Description                                                                                    |
|:-------------------- |:---------------------------------------------------------------------------------------------- |
| name                 | Structure name. Naming convention: 'mon' + mon_num                                             |
| mon_num              | Number for identification. Monomers in the central unit cell of the supercell are listed first |
| mon_type             | Monomers with the same mon_type have an identical geometry and only one needs to be calculated |
| calculate            | 'T': needs to be calculated, 'F': calculation done, 'N' calculation not necessary              |
| low_energy           | Monomer energy at the low-level in eV                                                          |
| high_energy          | Monomoer energy at the high-level in eV                                                        |
| data.low_forces      | Monomer forces at the low-level in eV/A                                                        |
| data.high_forces     | Monomer forces at the high-level in eV/A                                                       |
| data.sc_sector       | Unit cell in the supercell to which the monomer belongs                                        |
| data.indices         | Maps indices of the atoms in the monomer to the correct indices of the atoms of the supercell  |
| data.rotation_matrix | Matrix that rotates the monomer to fit the monomer of the same mon_type that is calculated     |

## Dimers
| Database field       | Description                                                                                    |
|:-------------------- |:---------------------------------------------------------------------------------------------- |
| name                 | Structure name. Naming convention: 'dim' + dim_num                                             |
| dim_num              | Number for identification                                                                      |
| dim_type             | Dimers with the same dim_type have an identical geometry and only one needs to be calculated   |
| dim_dist             | Distance between the two monomers                                                              |
| calculate            | 'T': needs to be calculated, 'F': calculation done, 'N' calculation not necessary              |
| low_energy           | Dimer energy at the low-level in eV                                                            |
| high_energy          | Dimer energy at the high-level in eV                                                           |
| mon_a_name           | Name of the first monomer                                                                      |
| mon_a_type           | Monomer type of the first monomer                                                              |
| mon_a_n              | Number of atoms of the first monomer                                                           |
| mon_b_name           | Name of the second monomer                                                                     |
| mon_b_type           | Monomer type of the second monomer                                                             |
| mon_b_n              | Number of atoms of the second monomer                                                          |
| data.low_forces      | Dimer forces at the low-level in eV/A                                                          |
| data.high_forces     | Dimer forces at the high-level in eV/A                                                         |
| data.indices         | Maps indices of the atoms in the dimer to the correct indices of the atoms of the supercell    |
| data.rotation_matrix | Matrix that rotates the dimer to fit the dimer of the same dim_type that is calculated         |
| data.mon_a_sc_sector | Unit cell in the supercell to which the first monomer belongs                                  |
| data.mon_b_sc_sector | Unit cell in the supercell to which the second monomer belongs                                 |

## Trimers
| Database field       | Description                                                                                    |
|:-------------------- |:---------------------------------------------------------------------------------------------- |
| name                 | Structure name. Naming convention: 'tri' + tri_num                                             |
| tri_num              | Number for identification                                                                      |
| tri_type             | Trimers with the same tri_type have an identical geometry and only one needs to be calculated  |
| calculate            | 'T': needs to be calculated, 'F': calculation done, 'N' calculation not necessary              |
| low_energy           | Trimer energy at the low-level in eV                                                           |
| high_energy          | Trimer energy at the high-level in eV                                                          |
| mon_a_name           | Name of the first monomer                                                                      |
| mon_a_type           | Monomer type of the first monomer                                                              |
| mon_a_n              | Number of atoms of the first monomer                                                           |
| mon_b_name           | Name of the second monomer                                                                     |
| mon_b_type           | Monomer type of the second monomer                                                             |
| mon_b_n              | Number of atoms of the second monomer                                                          |
| mon_c_name           | Name of the third monomer                                                                      |
| mon_c_type           | Monomer type of the third monomer                                                              |
| mon_c_n              | Number of atoms of the third monomer                                                           |
| dim_ab_name          | Name of the first dimer                                                                        |
| dim_ab_type          | Dimer type of the first dimer                                                                  |
| dim_dist_ab          | Distance between the molecules of the first dimer                                              |
| dim_ac_name          | Name of the second dimer                                                                       |
| dim_ac_type          | Dimer type of the second dimer                                                                 |
| dim_dist_ac          | Distance between the molecules of the second dimer                                             |
| dim_bc_name          | Name of the third dimer                                                                        |
| dim_bc_type          | Dimer type of the third dimer                                                                  |
| dim_dist_bc          | Distance between the molecules of the third dimer                                              |
| data.indices         | Maps indices of the atoms in the trimer to the correct indices of the atoms of the supercell   |
| data.rotation_matrix | Matrix that rotates the trimer to fit the trimer of the same tri_type that is calculated       |
| data.mon_a_sc_sector | Unit cell in the supercell to which the first monomer belongs                                  |
| data.mon_b_sc_sector | Unit cell in the supercell to which the second monomer belongs                                 |
| data.mon_c_sc_sector | Unit cell in the supercell to which the third monomer belongs                                  |

## Supercells containing finitely displaced atoms
| Database field       | Description                                                                                    |
|:-------------------- |:---------------------------------------------------------------------------------------------- |
| name                 | Structure name. Naming convention: 'displacement' + displacement_num                           |
| displacement_num     | Number for identification                                                                      |
| displaced_atom       | Index of displaced atom                                                                        |
| low_energy           | Energy of the low-level periodic calculation in eV                                             |
| data.displacement    | List containing index of displaced atom and displacement magnitude in each direction           |
| data.low_forces      | Forces of the low-level periodic calculation in eV/A                                           |
| data.high_forces     | Through embedding approximated high-level forces in eV/A                                       |

## Monomers containing finitely displaced atoms
| Database field              | Description                                                                             |
|:--------------------------- |:--------------------------------------------------------------------------------------- |
| name                        | Structure name. Naming convention: 'disp_mon' + disp_mon_num                            |
| disp_mon_num                | Number for identification                                                               |
| disp_id                     | ID of the displaced supercell (displacement_num) the displaced monomer belongs to       |
| displaced_atom              | Index of displaced atom                                                                 |
| orig_monomer                | Name of the original monomer                                                            |
| calculate                   | 'T': needs to be calculated, 'F': calculation done                                      |
| low_energy                  | Monomer energy at the low-level in eV                                                   |
| high_energy                 | Monomer energy at the high-level in eV                                                  |
| data.low_forces             | Monomer forces at the low-level in eV/A                                                 |
| data.high_forces            | Monomer forces at the high-level in eV/A                                                |
| data.displacement_magnitude | List containing displacement magnitude of the displaced atom in each direction          |
| data.indices                | Identical to data.indices of the original monomer                                       |

## Dimers containing finitely displaced atoms
| Database field              | Description                                                                             |
|:--------------------        |:--------------------------------------------------------------------------------------- |
| name                        | Structure name. Naming convention: 'disp_dim' + disp_dim_num                            |
| disp_dim_num                | Number for identification                                                               |
| disp_id                     | ID of the displaced supercell (displacement_num) the displaced monomer belongs to       |
| displaced_atom              | Index of displaced atom                                                                 |
| orig_dimer                  | Name of the original dimer                                                              |
| orig_mon_a                  | Name of the first original monomer                                                      |
| orig_mon_b                  | Name of the second original monomer                                                     |
| calculate                   | 'T': needs to be calculated, 'F': calculation done                                      |
| low_energy                  | Dimer energy at the low-level in eV                                                     |
| high_energy                 | Dimer energy at the high-level in eV                                                    |
| data.low_forces             | Dimer forces at the low-level in eV/A                                                   |
| data.high_forces            | Dimer forces at the high-level in eV/A                                                  |
| data.displacement_magnitude | List containing displacement magnitude of the displaced atom in each direction          |
| data.indices                | Identical to data.indices of the original dimer                                         |
| data.mon_b_sc_sector        | Unit cell in the supercell to which the second monomer belongs                          |





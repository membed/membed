# MEmbed input
The input file for MEmbed (membed.in) consists of key-value pairs, separated by whitespace.
One key-value pair per line.
The MEmbed-specific keywords are shown in the table below.

Every other key-value pair will be interpreted as setting for the calculations and handed to the respective ASE calculator.

If the value of a setting contains whitespace, the value has to be put in quotes ('' or "").  
Example: `aims_command    "srun aims.210716_2.scalapack.mpi.x"`

Settings can be applied for certain types of calculations only (periodic/monomers/dimers/trimers calculation, high/low level) by adding the respective system (per/mon/dim/tri) and/or level (hi/lo) at the end of the line containing the setting.  
Examples: `k_grid    "4 4 4"    per`, `xc pbe lo`, `xc pbe0 hi`  
If the same setting is given multiple times this way, the more specific one overwrites the more general one for the respective calculation.

When performing a phonon calculation, a standard single point calculation is carried out first, followed by the calculation of the displaced supercells and displaced multimers.
Settings that apply only for the calculation of displaced structures can be specified by adding 'vib_' in front of the key.
This is useful if, for example, the periodic calculation of the supercells requires a smaller k-grid than the periodic calculation of the unit cell:  
`k_grid    "4 4 4"    per`, `vib_k_grid    "2 2 2"    per`


## MEmbed keywords
| Keyword             | Required              | Allowed values/value types                              | Default value |
| -------------       |:---------------------:|:-------------------------------------------------------:|:-------------:|
| multimer_order      | yes                   | 0, 1, 2, 3                                              | -             |
| atoms_per_molecule  | yes                   | int                                                     | -             |
| calculation_type    | yes                   | 'sp', 'opt_full' or 'opt', 'opt_int', 'vib' or 'phonon' | -             |
| calculator          | yes                   | 'aims'                                                  | -             |
| multimer_cutoff     | if multimer_order > 1 | int, float                                              | -             |
| geometry_file_name  | no                    | string                                                  | 'geometry.in' |
| identify_fragments  | no                    | string                                                  | 'automatic'   |
| symmetrize          | no                    | bool                                                    | False         |
| compute_forces      | no                    | bool                                                    | \*            |
| compute_stress      | no                    | bool                                                    | \*\*          |
| f_max               | if optimization       | int, float                                              | -             |
| vib_sc_size         | if phonon calc.       | "int int int"                                           | -             |
| vib_q_grid          | no                    | "int int int"                                           | 'none'        |
| vib_disp            | no                    | int, float                                              | 0.005         |

\* True for all optimizations and phonon calculations, False for single points  
\*\* True for optimizations that include lattice optimization, False for single points, internal optimizations, phonon calculations

**multimer_order:** Order for up to which multimers will be generated. 0 (just periodic calculation), 1 (monomers), 2 (dimers), 3 (trimers).

**atoms_per_molecule:** Number of atoms per molecule. MEmbed currently allows only crystals with one type of molecule.

**calculation_type:** Type of calculation to be carried out. MEmbed can perform single point calculations ('sp'), geometry optimizations including ('opt_full' or 'opt') or excluding ('opt_int') lattice optimization, and phonon calculations ('vib' or 'phonon').

**calculator:** Program with which the calculations are performed. Currently only supports FHI-aims.

**multimer_cutoff:** Maximum distance between molecules of a dimer/trimer in Angstrom for it to be included in the calculation. Can be set to different values for dimers and trimers (see above).

**geometry_file_name:** Name of the file with the geometry information.

**identify_fragments:** If set to 'automatic', automatically identifies which atom belongs to which molecule. Can otherwise be set to the name of a file containing the fragment information (see example file 'frag.txt'). **Note that the input structure has to be unwrapped if the fragment identification is not done automatically!**

**symmetrize:** Fix symmetry during optimizations. **Highly recommended.**

**compute_forces:** Calculate atomic forces.

**compute_stress:** Calculate stress tensor.

**f_max:** Highest allowed force component for optimizations to terminate in eV/Angstrom.

**vib_sc_size:** Supercell dimensions for the phonon calculation.

**vib_q_grid:** q-grid for mesh sampling in reciprocal space. If given, MEmbed uses this q-grid for the calculation of Helmholtz free energies at 0 and 300 K. Additional thermal properties are saved to the membed.db database. However, it is recommended to use Phonopy for the evaluation of the phonon calculation.

**vib_disp:** Amplitude of the finite displacements for the phonon calculation in Angstrom.  




#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from math import ceil
from ase.db import connect
from ase.calculators.aims import Aims
from ase import Atoms, Atom
from .database import update_database, synchronize_database
from copy import deepcopy


def run_calculations(settings_orig):
    """Search database for necessary calculations and carry them out with the respective calculator"""

    db = connect("membed.db")
    settings = deepcopy(settings_orig)
    mult_names = {"mon": "monomer", "dim": "dimer", "tri": "trimer"}

    # Periodic calculation
    crystal_row = db.get(name="crystal")
    if crystal_row.get("low_energy") == None:
        print("  Performing low-level periodic calculation...", flush=True, end=" ")
        globals()[settings["per_lo"]["calculator"]](crystal_row, settings["per_lo"], settings["membed"]["compute_forces"], settings["membed"]["compute_stress"])
        print("Done.", end="")

    # Periodic calculation of displaced supercells
    current_nr = 0
    for row in db.select("displacement_num"):
        if row.get("low_energy") == None:
            if current_nr == 0:
                print("\n  Performing low-level periodic calculation of displaced supercells: ", flush=True, end="")
            current_nr += 1
            print(current_nr, flush=True, end=" ")
            settings["per_lo"]["directory"] = "vib/disp-" + str(row.displacement_num).zfill(3)
            globals()[settings["per_lo"]["calculator"]](row, settings["per_lo"], settings["membed"]["compute_forces"], settings["membed"]["compute_stress"])

    # Multimer calculation
    previous_mult = "none"
    for row in db.select(calculate="T"):
        name = row.name
        if name.startswith("disp_"):
            name = name[5:]
        mult = name[0:3]
        if not mult == previous_mult:
            print("\n  Performing low- and high-level " + mult_names[mult] + " calculations: ", flush=True, end="")
            current_nr = 0
        current_nr += 1
        print(current_nr, flush=True, end=" ")
        globals()[settings[mult + "_lo"]["calculator"]](row, settings[mult + "_lo"], settings["membed"]["compute_forces"], settings["membed"]["compute_stress"])
        globals()[settings[mult + "_hi"]["calculator"]](row, settings[mult + "_hi"], settings["membed"]["compute_forces"], settings["membed"]["compute_stress"])
        previous_mult = mult

    print("\n\n--------------------------------------------------------------------------------\n")


def aims(atoms_row, settings_orig, forces=False, stress=False):
    """Setup and run calculation with FHI-aims"""

    # Remove settings not to be handed to the aims calculator
    settings = deepcopy(settings_orig)
    level = settings.pop("level")
    system = settings.pop("system")
    calculator = settings.pop("calculator")
    if "multimer_cutoff" in settings:
        cutoff = settings.pop("multimer_cutoff")

    # Remove settings which were defined for vibrational calculations
    # (If this is a vibrational calculation, the settings will have already been renamed)
    for setting in list(settings):
        if setting.startswith("vib_"):
            settings.pop(setting)

    outfilename = atoms_row.name + "_"  + level + ".out"
    atoms = atoms_row.toatoms()

    # Prepare settings dictionary to be handed over to calculator
    settings["outfilename"] = outfilename
    settings["compute_forces"] = forces
    if system == "periodic":
        settings["compute_analytical_stress"] = stress
    else:
        settings["compute_analytical_stress"] = False

    calc = Aims(**settings)
    atoms.calc = calc

    calculation_result = []
    calculation_result.append([level + "_energy", atoms.get_total_energy()])
    if forces or stress:
        calculation_result.append([level + "_forces", atoms.get_forces()])
        if stress and system == "periodic":
            calculation_result.append([level + "_stress", atoms.get_stress(voigt=False)])

    update_database(atoms_row.name, calculation_result)


#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import spglib
from ase import Atoms, Atom
import numpy as np
from ase.build import make_supercell, sort
from ase import neighborlist
from scipy import sparse
from copy import deepcopy
from math import sqrt
from .output import print_error

def unwrap(atoms):
    """Unwrap atoms which have been wrapped by the symmetrization by using the text file
    created by 'determine_relative_position_correction'

    Check if atoms crossed the unit cell border and update position correction accordingly

    Update text file (determine_relative_position_correction is not called during optimizations)
    """

    pos_correction = np.loadtxt("pos_corr.tmp")
    rel_positions = atoms.get_scaled_positions(wrap=True)
    rel_positions_unwrapped = np.zeros((len(atoms),3),float)
    for i in range(len(rel_positions)):
        for j in range(len(rel_positions[i])):
            pos_old = None
            pos_old = pos_correction[i][j+3]
            pos_new = None
            pos_new = rel_positions[i][j] + pos_correction[i][j]
            delta = None
            delta = pos_new - pos_old
            if delta > 0.5:
                pos_correction[i][j] = pos_correction[i][j] - 1
            elif delta < -0.5:
                pos_correction[i][j] = pos_correction[i][j] + 1

            rel_positions_unwrapped[i][j] = rel_positions[i][j] + pos_correction[i][j]
            pos_correction[i][j+3] = rel_positions_unwrapped[i][j]

    atoms.set_scaled_positions(rel_positions_unwrapped)

    np.savetxt("pos_corr.tmp", pos_correction)

    return atoms


def get_supercell(atoms, cutoff):
    """Create supercell of sufficient size for the identification of multimers

    For each direction, calculate how many unit cells are needed so that a
    parallelepiped of height 'cutoff' would fit between the atoms of the
    central unit cell and the atoms of the next addition of a unit cell.
    """

    cell_parameters = atoms.cell.cellpar()
    rel_atom_positions = atoms.get_scaled_positions(wrap=False)

    # Determine the relative positions of the atoms furthest out
    rel_pos_min = [0] * 3
    rel_pos_max = [1] * 3
    for i in range(len(rel_atom_positions)):
        for j in range(3):
            if rel_atom_positions[i][j] < rel_pos_min[j]:
                rel_pos_min[j] = rel_atom_positions[i][j]
            if rel_atom_positions[i][j] > rel_pos_max[j]:
                rel_pos_max[j] = rel_atom_positions[i][j]

    # Determine relative lenghts spanned by the atoms furthest out
    len_total = [0.0] * 3
    for i in range(3):
        len_total[i] = rel_pos_max[i] - rel_pos_min[i]

    alpha = np.deg2rad(cell_parameters[3])
    beta = np.deg2rad(cell_parameters[4])
    gamma = np.deg2rad(cell_parameters[5])

    # Use volume formula for parallelepiped to calculate edge lengths of parallelepiped with height 'cutoff'
    factor = sqrt(1 + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma) - np.cos(alpha)**2 - np.cos(beta)**2 - np.cos(gamma)**2)
    cutoff_scaled = [0.0] * 3
    cutoff_scaled[0] = cutoff * np.sin(alpha) / factor
    cutoff_scaled[1] = cutoff * np.sin(beta) / factor
    cutoff_scaled[2] = cutoff * np.sin(gamma) / factor

    # Determine necessary size of super cell
    sc_dimensions = [None] * 3
    for i in range(3):
        n = (len_total[i] * cell_parameters[i] + cutoff_scaled[i]) // cell_parameters[i]
        sc_dimensions[i] = int(2 * n + 1)

    # Create supercell
    supercell = make_supercell(atoms, [[sc_dimensions[0], 0, 0], [0, sc_dimensions[1], 0], [0, 0, sc_dimensions[2]]], wrap = False)
    print("\n--------------------------------------------------------------------------------\n", flush=True)
    print("  Generated a " + str(sc_dimensions[0]) + "*" + str(sc_dimensions[1]) + "*" + str(sc_dimensions[2]) + " supercell for multimer identification.", flush = True)

    return supercell, sc_dimensions


def assign_atoms_to_fragments(supercell, fragments, n_uc):
    """Assign atoms in supercell their respective fragment number and save the
    information as fragments attribute to the supercell object
    """

    supercell.fragments = [None] * len(supercell)
    for i in range(len(supercell)):
        supercell.fragments[i] = i // n_uc * max(fragments) + fragments[i % n_uc]

    return supercell


def determine_relative_position_correction(atoms):
    """Identify whether atoms are lying inside or outside of the unit cell and create
    text file containing the position corrections and relative positions

    The text file is needed to unwrap the crystal structure between optimization
    steps, since the symmetrization always wraps the structure.
    """

    pos_correction = np.zeros((len(atoms)+1,6), dtype=float) # len(atoms)+1 allows for interpretation as array even if only one atom in unit cell
    for i, pos in enumerate(atoms.get_scaled_positions(wrap=False)):
        for j in range(len(pos)):
            if pos[j] > 1:
                pos_correction[i][j] = 1
            elif pos[j] < 0:
                pos_correction[i][j] = -1
            else:
                pos_correction[i][j] = 0
            pos_correction[i][j+3] = pos[j]

    np.savetxt("pos_corr.tmp", pos_correction)


def symmetrize_crystal_structure(atoms, symprec=0.05):
    """Symmetrize a crystal structure given in atoms (requires spglib)"""

    cell = (atoms.get_cell()).tolist()
    positions = atoms.get_scaled_positions().tolist()
    atomic_numbers = atoms.get_atomic_numbers()

    new_cell, new_scaled_positions, atomic_numbers = spglib.standardize_cell(
        (cell, positions, atomic_numbers), to_primitive=False,
        no_idealize=True, symprec=symprec)

    atoms.set_cell(new_cell)
    atoms.set_scaled_positions(new_scaled_positions)
    atoms.set_atomic_numbers(atomic_numbers)


def identify_fragments(atoms_orig, atoms_per_molecule):
    """Identify which atoms belong to the same fragment

    - Unwrap structure by generating a 5x5x5 supercell
    - Identify connected atoms
    - Delete molecules whose center of mass does not lie within the central unit cell
    """

    atoms = deepcopy(atoms_orig)

    # Create supercell. atoms.tag is later needed to recreate the original atom order
    supercell = make_supercell(atoms, [[5,0,0], [0,5,0], [0,0,5]], wrap = True)
    supercell.set_pbc(False)
    supercell.set_cell(None)
    for i in range(len(supercell)):
        supercell[i].tag = i % len(atoms_orig)

    # Identify connected atoms
    cutOff = neighborlist.natural_cutoffs(supercell, mult=0.8)
    neighborList = neighborlist.NeighborList(cutOff, self_interaction=False, bothways=True)
    neighborList.update(supercell)
    matrix = neighborList.get_connectivity_matrix(sparse=False)
    n_components, component_list = sparse.csgraph.connected_components(matrix)

    # Identify fragment numbers with the correct number of atoms
    whole_fragment_numbers = []
    for i in range(n_components):
        n_atoms_per_frag = 0
        for j in range(len(component_list)):
            if i == component_list[j]:
                n_atoms_per_frag += 1
        if n_atoms_per_frag == atoms_per_molecule:
            whole_fragment_numbers.append(i)

    # Move supercell so that the central unit cell touches the origin
    cell = atoms.get_cell()
    for cell_vector in cell:
        supercell.translate(-2*cell_vector)
    # Crystal structures often have atoms on the edge of the unit cell
    # This small translation prevents machine precision roulette
    supercell.translate([1e-10, 1e-10, 1e-10])

    # Extract whole fragments from the supercell
    fragments = []
    for i in range(len(whole_fragment_numbers)):
        fragment = None
        fragment = deepcopy(supercell)
        del fragment[[j for j in range(len(fragment)) if (component_list[j] != whole_fragment_numbers[i])]]
        fragments.append(fragment)

    # Empty original atoms object
    del atoms[[atom.index for atom in atoms if atom.index in range(int(len(atoms)))]]

    # Add fragments back to the atoms object if their center of mass lies within the unit cell
    for i in range(len(fragments)):
        com_fragment = fragments[i].get_center_of_mass()
        dummy_atom = Atom('X', (com_fragment[0], com_fragment[1], com_fragment[2]))
        atoms.append(dummy_atom)
        com_rel_pos = atoms.get_scaled_positions(wrap=False)[-1]
        inside_cell = True
        for j in range(3):
            if com_rel_pos[j] < 0 or com_rel_pos[j] >= 1:
                inside_cell = False
        if inside_cell:
            atoms.extend(fragments[i])

    # Delete all dummy atoms and revert the small translation from earlier
    del atoms[[atom.index for atom in atoms if atom.symbol=='X']]
    atoms.translate([-1e-10, -1e-10, -1e-10])

    # Check if the right amount of fragments were identified and kept
    if not len(atoms) == len(atoms_orig):
        print_error("Error with unwrapping input structure. Unwrapped structure does not contain same number of molecules as wrapped structure.")
    
    # Sort the atoms object so that the atom order is identical to the original one. Change fragment list accordingly
    tags = []
    fragments = []
    for i in range(len(atoms)):
        tags.append(atoms[i].tag)
        fragments.append(i // atoms_per_molecule + 1)
    atoms_sorted = sort(atoms, tags)
    fragments_sorted = [f for t, f in sorted(zip(tags, fragments))]

    # Use the unwrapped crystal structure to determine if atoms lie outside of the unit cell
    determine_relative_position_correction(atoms_sorted)

    return(fragments_sorted)


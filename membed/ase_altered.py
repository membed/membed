#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
#
# This file contains modified functions from the Atomic Simulation
# Environment (ASE) - see https://gitlab.com/ase/ase


from math import sqrt
from ase.build.rotate import rotation_matrix_from_points
import numpy as np
from ase import Atoms, Atom


def minimize_rotation_and_translation_altered(target,atoms):
    """Minimize RMSD between atoms and target.
    
    Rotate and translate atoms to best match target.  For more details, see::
        
        Melander et al. J. Chem. Theory Comput., 2015, 11,1055

    Changed: returns the rotation matrix.
    """

    p = atoms.get_positions()
    p0 = target.get_positions()

    # centeroids to origin
    c = np.mean(p, axis=0)
    p -= c
    c0 = np.mean(p0, axis=0)
    p0 -= c0

    # Compute rotation matrix
    R = rotation_matrix_from_points(p.T, p0.T)

    atoms.set_positions(np.dot(p, R.T) + c0)

    return R


def distance_altered(s1, s2, permute=True):
    """Get the distance between two structures s1 and s2.
    
    The distance is defined by the Frobenius norm of
    the spatial distance between all coordinates (see
    numpy.linalg.norm for the definition).

    permute: minimise the distance by 'permuting' same elements

    Changed: save and return atom index order of best permutation
    """

    s1 = s1.copy()
    s2 = s2.copy()
    for s in [s1, s2]:
        s.translate(-s.get_center_of_mass())
    s2pos = 1. * s2.get_positions()
    
    def align(struct, xaxis='x', yaxis='y'):
        """Align moments of inertia with the coordinate system."""
        Is, Vs = struct.get_moments_of_inertia(True)
        IV = list(zip(Is, Vs))
        IV.sort(key=lambda x: x[0])
        struct.rotate(IV[0][1], xaxis)
        
        Is, Vs = struct.get_moments_of_inertia(True)
        IV = list(zip(Is, Vs))
        IV.sort(key=lambda x: x[0])
        struct.rotate(IV[1][1], yaxis)

    align(s1)

    def dd(s1, s2, permute):
        if permute:
            s2 = s2.copy()
            dist = 0
            imin = [None] * len(s1)
            for j, a in enumerate(s1):
                dmin = np.Inf
                for i, b in enumerate(s2):
                    if a.symbol == b.symbol:
                        d = np.sum((a.position - b.position)**2)
                        if d < dmin:
                            dmin = d
                            imin[j] = i
                dist += dmin
                #s2.pop(imin[j])
                s2[imin[j]].position = [10000, 10000, 10000]
            return np.sqrt(dist), imin
        else:
            return np.linalg.norm(s1.get_positions() - s2.get_positions())

    dists = []
    indices = []
    # principles
    for x, y in zip(['x', '-x', 'x', '-x'], ['y', 'y', '-y', '-y']):
        s2.set_positions(s2pos)
        align(s2, x, y)
        dist = None
        index = None
        dist, index = dd(s1, s2, permute)
        dists.append(dist)
        indices.append(index)
   
    return min(dists), indices[np.argmin(dists)]


#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from math import sqrt, factorial
from copy import deepcopy
from ase.geometry import get_distances
import numpy as np
from ase import Atoms, Atom
import itertools
from .ase_altered import minimize_rotation_and_translation_altered, distance_altered


def get_monomers(supercell, sc_dimensions, n_uc, n_fragments_uc, generate_supercell_monomers):
    """Generate all monomers in the supercell"""

    # Lower and upper index specifiy the fragment numbers of fragments in the central unit cell
    lower_index = (sc_dimensions[0] * sc_dimensions[1] * sc_dimensions[2] // 2) * n_uc
    upper_index = lower_index + n_uc
    monomers = []
    mon_num = 1

    # Atoms.atom.tag is used to later assign each atom of the fragments to the correct atom in the unit cell
    for i in range(len(supercell)):
        supercell[i].tag = supercell[i].index - lower_index

    # Delete all atoms which do not belong to the respective fragment
    for i in range(max(supercell.fragments)):
        tmp = None
        tmp = deepcopy(supercell)
        del tmp[[j for j in range(len(tmp)) if (tmp.fragments[j] != i+1)]]
        tmp.set_pbc(False)
        tmp.set_cell(None)

        # In which cell does the monomer lie? Sector [0,0,0] is the central unit cell.
        n_cells_yz = sc_dimensions[1] * sc_dimensions[2]
        n_cells_z = sc_dimensions[2]
        sc_sector_nr = i // n_fragments_uc
        sc_sector_x = (sc_sector_nr // n_cells_yz) + 1
        sc_sector_y = ((sc_sector_nr % n_cells_yz) // n_cells_z) + 1
        sc_sector_z = (((sc_sector_nr % n_cells_yz) % n_cells_z) // 1) + 1
        sc_sector_x_rel = sc_sector_x - (sc_dimensions[0] // 2 + 1)
        sc_sector_y_rel = sc_sector_y - (sc_dimensions[1] // 2 + 1)
        sc_sector_z_rel = sc_sector_z - (sc_dimensions[2] // 2 + 1)
        sc_sector = [sc_sector_x_rel, sc_sector_y_rel, sc_sector_z_rel]
        tmp.sc_sector = sc_sector

        # Put the monomers in the central unit cell in front of the list
        if i+1 in range(supercell.fragments[lower_index], supercell.fragments[upper_index]):
            monomers.insert(mon_num - 1, tmp)
            mon_num = mon_num + 1
        elif generate_supercell_monomers:
            monomers.append(tmp)

    return monomers


def get_dimers(monomers, cut_dim, n_molecules, generate_supercell_dimers):
    """Generate all dimers within the dimer cutoff"""

    dimers = []
    for i_idx, i_mon in enumerate(monomers):
        for j_idx, j_mon in enumerate(monomers):
            if (j_idx > i_idx) and (i_idx < n_molecules or generate_supercell_dimers):
                pos1 = None
                pos2 = None
                dist_matrix = None
                min_per_atom = []
                min_distance = None
                pos1 = i_mon.get_positions()
                pos2 = j_mon.get_positions()
                dist_matrix = get_distances(pos1,pos2)[1]
                for k in dist_matrix:
                    min_per_atom.append(min(k))
                min_distance = min(min_per_atom)
                if min_distance <= cut_dim and min_distance >= 0.1:
                    dimer = None
                    dimer = i_mon + j_mon
                    dimer.mon_a_n = len(i_mon)
                    dimer.mon_b_n = len(j_mon)
                    dimer.mon_a_type = i_mon.multimer_type
                    dimer.mon_b_type = j_mon.multimer_type
                    dimer.dim_dist = min_distance
                    dimer.mon_a_name = "mon%s" % str(i_idx+1)
                    dimer.mon_b_name = "mon%s" % str(j_idx+1)
                    dimer.mon_a_sc_sector = i_mon.sc_sector
                    dimer.mon_b_sc_sector = j_mon.sc_sector
                    dimers.append(dimer)
    return dimers


def get_trimers(monomers, dimers, cut_tri, n_molecules, generate_supercell_trimers):
    """Generate all trimers within the trimer cutoff"""

    trimers = []
    for dim_idx, dim in enumerate(dimers):
        mon_a_idx = int(dim.mon_a_name[3:]) - 1
        mon_b_idx = int(dim.mon_b_name[3:]) - 1
        mon_a = monomers[mon_a_idx]
        mon_b = monomers[mon_b_idx]
        for mon_c_idx, mon_c in enumerate(monomers):
            if (mon_c_idx > mon_b_idx) and (mon_a_idx < n_molecules or generate_supercell_trimers):
                positions = [mon_a.get_positions(), mon_b.get_positions(), mon_c.get_positions()]
                dist_matrices = [get_distances(positions[0], positions[1])[1], get_distances(positions[0], positions[2])[1], get_distances(positions[1], positions[2])[1]]
                min_distances = [dist_matrices[0].min(), dist_matrices[1].min(), dist_matrices[2].min()]
                nr_within_cutoff = 0
                for dist in min_distances:
                    if dist < cut_tri:
                        nr_within_cutoff += 1

                if nr_within_cutoff >= 3:
                    trimer = None
                    trimer = dim + mon_c
                    trimer.mon_a_n = dim.mon_a_n
                    trimer.mon_b_n = dim.mon_b_n
                    trimer.mon_c_n = len(mon_c)
                    trimer.mon_a_type = dim.mon_a_type
                    trimer.mon_b_type = dim.mon_b_type
                    trimer.mon_c_type = mon_c.multimer_type
                    trimer.dim_dists = min_distances
                    trimer.mon_a_name = dim.mon_a_name
                    trimer.mon_b_name = dim.mon_b_name
                    trimer.mon_c_name = "mon%s" % str(mon_c_idx+1)
                    trimer.mon_a_sc_sector = dim.mon_a_sc_sector
                    trimer.mon_b_sc_sector = dim.mon_b_sc_sector
                    trimer.mon_c_sc_sector = mon_c.sc_sector
                    trimer.dim_ab_name = "dim%s" % str(dim_idx+1)
                    trimer.dim_ab_type = dim.multimer_type

                    # Identify the two dimers that arise from the addition of the monomer to the dimer
                    for dimer_idx, dimer in enumerate(dimers):
                        if dimer.mon_a_name == trimer.mon_a_name and dimer.mon_b_name == trimer.mon_c_name:
                            trimer.dim_ac_name = "dim%s" % str(dimer_idx+1)
                            trimer.dim_ac_type = dimer.multimer_type
                        elif dimer.mon_a_name == trimer.mon_b_name and dimer.mon_b_name == trimer.mon_c_name:
                            trimer.dim_bc_name = "dim%s" % str(dimer_idx+1)
                            trimer.dim_bc_type = dimer.multimer_type

                    trimers.append(trimer)
    return trimers


def get_duplicates(multimers):
    """Determine unique multimers and multimers which are identical to them (through translation and rotation)

    Only unique multimers have to be calculated later, the results can then be copied to the non-unique ones
    """

    for i in range(len(multimers)):
        multimers[i].multimer_type = None

    multimer_type = 1
    for i in range(len(multimers)):
        # If the multimer has no multimer type yet, it is a unique multimer
        if multimers[i].multimer_type == None:
            multimers[i].multimer_type = multimer_type
            multimers[i].calculate = "T"
            multimers[i].rotation_matrix = None

            # Check all remaining multimers if they are identical to this new multimer type.
            for j in range(i+1, len(multimers)):
                if multimers[j].multimer_type == None and multimers[i].get_chemical_formula() == multimers[j].get_chemical_formula():
                    m1 = None
                    m1 = deepcopy(multimers[i])
                    m2 = None
                    m2 = deepcopy(multimers[j])

                    # Use the (altered) distance function from ASE to find the atom permutation which gives the lowest RMSD
                    distance, indices = distance_altered(m1,m2,True)
                    for k in range(len(indices)):
                        m2.append(m2[indices[k]])
                    del m2[[atom.index for atom in m2 if atom.index in range(int(len(m2)/2))]]

                    # Calculate RMSD. Rotation matrix is needed to copy the forces from one multimer to another
                    m2_rotated = deepcopy(m2)
                    rotation_matrix = minimize_rotation_and_translation_altered(m1, m2_rotated)
                    dist_matrix = get_distances(m1.get_positions(), m2_rotated.get_positions())[1]
                    rmsd = None
                    msd = 0.0
                    for k in range(len(dist_matrix)):
                        msd = msd + (dist_matrix[k][k] ** 2)
                    rmsd = sqrt(msd / len(dist_matrix))

                    # If multimers are identical, set multimer type of the second multimer to the one of the first
                    if rmsd < 1e-10:
                        multimers[j].multimer_type = multimer_type
                        multimers[j].calculate = "N"
                        multimers[j].rotation_matrix = rotation_matrix
                        multimers[j].extend(m2)
                        del multimers[j][[atom.index for atom in multimers[j] if atom.index in range(int(len(multimers[j])/2))]]

            multimer_type = multimer_type + 1

        # For the RMSD calculation, atom order of the not-unique multimers was switched
        # Indices attribute holds the information to which atom in the unit cell the multimer atoms belong
        multimers[i].indices = []
        for j in range(len(multimers[i])):
            multimers[i].indices.append(int(multimers[i][j].tag))

    return multimers, multimer_type - 1


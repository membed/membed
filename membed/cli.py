#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


"""MEmbed main functions"""

from ase import Atoms, Atom
from ase.io import read, write
from ase.optimize import BFGS
from ase.constraints import ExpCellFilter
from ase.spacegroup.symmetrize import FixSymmetry
from ase.spacegroup import get_spacegroup
from .input import concatenate_input_strings, convert_input_variables, validate_input, convert_input_to_settings_dict
from .cell import determine_relative_position_correction, symmetrize_crystal_structure, identify_fragments
from .MEmbed import MEmbed
from .output import print_output_header, print_error, print_geometry
import os
import datetime

def main():
    """Main function"""

    # Print MEmbed output header
    print_output_header()
    
    # Print start date and time
    print("  Start date and time: "+str(datetime.datetime.now()))
    print("") 

    # Read geometry.in, membed.in, and (if specified) file with fragment info
    atoms, settings = parse_input()

    # Check if symmetrization is desired. Set symprec to the given value if the setting is not of type bool
    if settings["membed"]["symmetrize"] != False:
        symprec = 0.05
        if settings["membed"]["symmetrize"] != True:
            symprec = settings["membed"]["symmetrize"]
        #symmetrize_crystal_structure(atoms, symprec=symprec)

    # Identify fragments if necessary
    if settings["membed"]["identify_fragments"] == "automatic":
        fragments = identify_fragments(atoms, settings["membed"]["atoms_per_molecule"])
        settings["membed"]["fragments"] = fragments
    else:
        determine_relative_position_correction(atoms)
    
    # Initialize calculator
    atoms.wrap()
    calc = MEmbed(settings=settings)
    atoms.calc = calc

    # Start the respective calculation
    calc_type = settings["membed"]["calculation_type"]
    if calc_type == "sp":
        e = atoms.get_total_energy()
    elif calc_type == "opt_int":
        if settings["membed"]["symmetrize"] != False:
            atoms.set_constraint(FixSymmetry(atoms, symprec=symprec))
        dyn = BFGS(atoms, trajectory="opt.traj")
        dyn.run(fmax=settings["membed"]["f_max"])
    elif calc_type == "opt_full" or calc_type == "opt":
        if settings["membed"]["symmetrize"] != False:
            atoms.set_constraint(FixSymmetry(atoms, symprec=symprec))
        ecf = ExpCellFilter(atoms)
        dyn = BFGS(ecf, trajectory="opt.traj")
        dyn.run(fmax=settings["membed"]["f_max"])
    elif calc_type == "vib":
        from .vib import vib_main
        e = atoms.get_total_energy()
        vib_main(settings)

    if calc_type == "opt_int" or calc_type == "opt_full" or calc_type == "opt":
        print("\n  Final geometry:")
        print_geometry(atoms)
    print("\n  Final embedding energy:    " + str('{0:.10f}'.format(round(atoms.get_total_energy(),10))).rjust(20) + " eV")

    # Remove temporary files
    temp_files = ["pos_corr.tmp", "calc/geometry.in", "calc/control.in", "calc/parameters.ase",
                 "vib/multimers_displaced/geometry.in", "vib/multimers_displaced/control.in", "vib/multimers_displaced/parameters.ase"]
    for temp_file in temp_files:
        if os.path.exists(temp_file):
            os.remove(temp_file)

    # Print exit statement
    print("\n--------------------------------------------------------------------------------")
    print("  MEmbed has terminated normally at "+str(datetime.datetime.now()))
    print("--------------------------------------------------------------------------------")


def parse_input():
    """Read input settings and geometry"""

    with open("membed.in") as f:
        lines = [line.partition('#')[0].rstrip() for line in f]
    input = list([line.split() for line in lines if line])
    input = concatenate_input_strings(input)
    input = convert_input_variables(input)
    validate_input(input)
    settings = convert_input_to_settings_dict(input)

    # Read geometry file
    atoms = read(settings["membed"]["geometry_file_name"])
    if len(atoms) == 0:
        print_error("No atoms were found in geometry file.")
    if not len(atoms) % settings["membed"]["atoms_per_molecule"] == 0:
        print_error("Number of atoms in the geometry file is not a whole multiple of the number of atoms per molecule specified.")
    rel_pos = atoms.get_scaled_positions(wrap=False)
    for i in range(len(rel_pos)):
        for j in range(len(rel_pos[i])):
            if rel_pos[i][j] >= 2 or rel_pos[i][j] <= -2:
                print_error("Found atom too far out of the unit cell. Relative atomic positions can only range from -2 to +2.")

    n_atoms = len(atoms)
    atoms_per_molecule = settings["membed"]["atoms_per_molecule"]
    n_molecules = n_atoms // atoms_per_molecule

    # Print input structure
    print("  Input structure:")
    with open(settings["membed"]["geometry_file_name"]) as f_instr:
        for line in f_instr:
            print("  " + line, end="")

    # Read the fragment information file if desired
    if not settings["membed"]["identify_fragments"] == "automatic":
        with open(str(settings["membed"]["identify_fragments"])) as f:
            lines = [line.rstrip() for line in f]
            lines = [line for line in lines if line]

        # Check if only one integer is given per line
        try:
            fragments = list(map(int, lines))
        except:
            print_error("A line from fragment information file '" + settings["membed"]["identify_fragments"] + "' could not be interpreted as an integer.")

        # Check if the numbers given go from 1 to *molecules per unit cell*
        for fragment in fragments:
            if fragment not in range(1, n_molecules + 1):
                print_error("Error with reading fragment information file '" + settings["membed"]["identify_fragments"]
                                + "'. Fragment numbers must range from 1 to " + str(n_molecules) + " (molecules per unit cell).")

        # Check if every fragment number appears *atoms_per_molecule* times
        for i in range(1, n_molecules + 1):
            if fragments.count(i) != atoms_per_molecule:
                print_error("Error with reading fragment information file '" + settings["membed"]["identify_fragments"] + "'. Fragment number "
                                + str(i) + " was not counted " + str(atoms_per_molecule) + " (atoms per molecule) times.")

        settings["membed"]["fragments"] = fragments

    return atoms, settings




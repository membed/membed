#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


def print_output_header():
    """Print the MEmbed output header"""
    
    print("--------------------------------------------------------------------------------")
    print("")
    print("  Starting")
    print("   __  __  ______             _                _ ")
    print("  |  \/  ||  ____|           | |              | |")
    print("  | \  / || |__    _ __ ___  | |__    ___   __| |")
    print("  | |\/| ||  __|  | '_ ` _ \ | '_ \  / _ \ / _` |")
    print("  | |  | || |____ | | | | | || |_) ||  __/| (_| |")
    print("  |_|  |_||______||_| |_| |_||_.__/  \___| \__,_|  Version 0.2.0")
    print("")
    print("  MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals")
    print("")
    print("  *  Code available at https://gitlab.com/membed/membed")
    print("")
    print("  *  When using MEmbed, please cite the following publication:")
    print("")
    print("  J. Hoja, A. List, A.D. Boese, A Multimer Embedding Approach for Molecular")
    print("  Crystals up to Harmonic Vibrational Properties, arXiv:2209.02687 (2022).")
    print("  https://doi.org/10.48550/arXiv.2209.02687")
    print("")
    print("") 
    print("  Copyright (C): 2021-2022 Johannes Hoja")
    print("                      2022 Alexander List")
    print("                      2022 A. Daniel Boese")          
    print("")
    print("  This program is free software: you can redistribute it and/or modify it")
    print("  under the terms of the GNU Lesser General Public License as published by")
    print("  the Free Software Foundation, either version 3 of the License, or (at")
    print("  your option) any later version.")
    print("")
    print("  This program is distributed in the hope that it will be useful, but")
    print("  WITHOUT ANY WARRANTY; without even the implied warranty of")
    print("  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser")
    print("  General Public License for more details.")
    print("")
    print("  You should have received a copy of the GNU Lesser General Public License")
    print("  along with this program. If not, see https://www.gnu.org/licenses/.")
    print("")
    print("")
    print("  The following software is needed for running MEmbed:")
    print("")
    print("  *  Atomic Simulation Environment (ASE): https://wiki.fysik.dtu.dk/ase/")
    print("  *  FHI-aims: https://fhi-aims.org or https://aims-git.rz-berlin.mpg.de")
    print("  *  Phonopy (for phonon calculations): https://phonopy.github.io/phonopy/")
    print("--------------------------------------------------------------------------------")
    print("")


def print_error(err_msg):
    """Print error message and raise an exception"""
    
    gen_err_msg = "MEmbed has encountered an error. For more information read the output file."

    print("   ______  _____   _____    ____   _____   ")
    print("  |  ____||  __ \ |  __ \  / __ \ |  __ \  ")
    print("  | |__   | |__) || |__) || |  | || |__) | ")
    print("  |  __|  |  _  / |  _  / | |  | ||  _  /  ")
    print("  | |____ | | \ \ | | \ \ | |__| || | \ \  ")
    print("  |______||_|  \_\|_|  \_\ \____/ |_|  \_\ ")
    print("")
    print("--------------------------------------------------------------------------------")
    print("ERROR: "+str(err_msg))
    print("--------------------------------------------------------------------------------")

    print("MEmbed has terminated abnormally! Exiting MEmbed.")
    print("--------------------------------------------------------------------------------")

    raise Exception(gen_err_msg)

def print_geometry(atoms):
    """Print the geometry of the atoms object"""
    for i in range(3):
        print("  lattice_vector", end="")
        for j in range(3):
            print(str('{0:.16f}'.format(round(atoms.cell[i][j],16))).rjust(20), end="")
        print("")
    pos = atoms.get_positions()
    ele = atoms.get_chemical_symbols()
    for i in range(len(atoms)):
        print("  atom", end="")
        for j in range(3):
            print(str('{0:.16f}'.format(round(pos[i][j],16))).rjust(20), end="")
        print("  " + ele[i])


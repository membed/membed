#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from copy import deepcopy
from .output import print_error

def concatenate_input_strings(input):
    """Concatenate the input chunks which were originally put between
    apostrophes in the input file

    Recognises "" as well as ''
    """

    for i in range(len(input)):
        for j in range(len(input[i])):
            if input[i][j].startswith("'") or input[i][j].startswith('"'):
                apostrophe = input[i][j][0]
                for k in range(j, len(input[i])):
                    if input[i][k].endswith(apostrophe):
                        concatenated = ""
                        for l in range(j, k+1):
                            concatenated += input[i][l] + " "
                            input[i][l] = "delete_me"
                        input[i][j] = concatenated[1:-2]
                        break
    
    for i in range(len(input)):
        input[i] = [ele for ele in input[i] if ele != "delete_me"]

    return input


def convert_input_variables(input):
    """Convert all pieces of input to the right data type or format

    - Convert the input chunks to lowercase
    - Try to convert input chunks to int, float, or bool
    - Try to replace input chunks by the partial setting keywords
      recognisable by MEmbed
    """
    
    partial_settings = ["per", "mon", "dim", "tri", "lo", "hi"]
    
    for i in range(len(input)):
        for j in range(len(input[i])):
            input[i][j] = input[i][j].lower()
            try:
                input[i][j] = int(input[i][j])
            except:
                try:
                    input[i][j] = float(input[i][j])
                except:
                    if input[i][j] == "true":
                        input[i][j] = True
                    elif input[i][j] == "false":
                        input[i][j] = False
                    else:
                        for part in partial_settings:
                            if input[i][j].startswith(part):
                                input[i][j] = part

    return input


def validate_input(input):
    """Check if all necessary keywords are set and have allowed values
    Set default values for keywords that are not set
    Check if there are combinations of settings that do not make sense
    """

                           # keyword                    required    allowed type            allowed val                                             default val
    allowed_membed_input = {"multimer_order":           [ True,     [int],                  [0, 1, 2, 3],                                           [],                  ],
                            "multimer_cutoff":          [ False,    [float, int],           [],                                                     [],                  ],
                            "atoms_per_molecule":       [ True,     [int],                  [],                                                     [],                  ],
                            "calculation_type":         [ True,     [str],                  ["sp", "opt", "opt_full", "opt_int", "vib", "phonon"],  [],                  ],
                            "calculator":               [ True,     [str],                  ["aims"],                                               [],                  ],
                            "geometry_file_name":       [ False,    [str],                  [],                                                     ["geometry.in"],     ],
                            "identify_fragments":       [ False,    [str],                  [],                                                     ["automatic"],       ],
                            "symmetrize":               [ False,    [bool, float, int],     [],                                                     [False],             ],
                            "f_max":                    [ False,    [float, int],           [],                                                     [],                  ],
                            "vib_sc_size":              [ False,    [str],                  [],                                                     [],                  ],
                            "vib_q_grid":               [ False,    [str],                  [],                                                     ["none"]             ],
                            "vib_disp":                 [ False,    [float, int],           [],                                                     [],                  ],
                            "compute_forces":           [ False,    [bool],                 [],                                                     [],                  ],
                            "compute_stress":           [ False,    [bool],                 [],                                                     [],                  ],
                            }
    allowed_partial_settings = {"systems": ["per", "mon", "dim", "tri"], "levels": ["lo", "hi"]}

    # Are all must-have keywords in the input file? Also set default values if optional keywords are not set
    for item in allowed_membed_input.items():
        if item[1][0]:
            if not item[0] in [entry[0] for entry in input]:
                print_error("Required keyword not found in input file: '" + item[0] + "'.")
        else:
            if (not item[0] in [entry[0] for entry in input]) and len(item[1][3]) > 0:
                input.append([item[0], item[1][3][0]])

    # Do the keywords have the allowed number of arguments?
    for entry in input:
        if entry[0] in allowed_membed_input:
            if len(entry) < 2:
                print_error("Missing an argument for keyword '" + entry[0] + "'.")
            elif len(entry) > 2:
                if (entry[0] == "multimer_cutoff" and len(entry) < 5) or (entry[0] == "calculator" and len(entry) < 5):
                    pass
                else:
                    print_error("Too many arguments for keyword '" + entry[0] + "'.")
        else:
            if len(entry) < 2:
                print_error("Missing an argument for keyword '" + entry[0] + "'. If the keyword takes no additional arguments, please put empty quotes as argument.")
            elif len(entry) > 4:
                print_error("Too many arguments for keyword '" + entry[0] + "'.")

    # Do the keyword arguments have allowed values?
    for entry in input:
        if entry[0] in allowed_membed_input:
            wrong_value = False
            if not type(entry[1]) in allowed_membed_input[entry[0]][1]:
                wrong_value = True
                err_msg_ending = ". Allowed are values of type "
                for allowed_type in allowed_membed_input[entry[0]][1]:
                    err_msg_ending += str(allowed_type) + ", "
            if len(allowed_membed_input[entry[0]][2]) > 0:
                err_msg_ending = ". Allowed values are "
                for allowed_value in allowed_membed_input[entry[0]][2]:
                    err_msg_ending += "'" + str(allowed_value) + "', "
                if not entry[1] in allowed_membed_input[entry[0]][2]:
                    wrong_value = True
            if wrong_value:
                err_msg_ending = err_msg_ending[:-2] + "."
                err_msg = "Not allowed value found for keyword " + entry[0] + err_msg_ending
                print_error(err_msg)

    # Do the partial settings only contain allowed strings? If two partial settings are set, do they belong to the two different setting groups (hi/low, per/mon/dim/...)?
    for entry in input:
        if len(entry) > 2:
            if (not entry[2] in allowed_partial_settings["systems"]) and (not entry[2] in allowed_partial_settings["levels"]):
                print_error("'" + str(entry[2]) + "' is not an allowed partial setting.")
            if len(entry) > 3:
                if (not entry[3] in allowed_partial_settings["systems"]) and (not entry[3] in allowed_partial_settings["levels"]):
                    print_error("'" + str(entry[3]) + "' is not an allowed partial setting.")
                if ((entry[2] in allowed_partial_settings["systems"] and entry[3] in allowed_partial_settings["systems"]) or
                    (entry[2] in allowed_partial_settings["levels"] and entry[3] in allowed_partial_settings["levels"])):
                    print_error("Partial settings of the same type (hi/low, per/mon/dim/etc.) are defined for setting '" + str(entry[0]) + "' in the same line.")

    # Are keywords set to the same type of calculation twice?
    for i in range(len(input)):
        for j in range(i+1, len(input)):
            if len(input[i]) == len(input[j]):
                if len(input[i]) == 2:
                    if input[i][0] == input[j][0]:
                        print_error("'" + input[i][0] + "' is defined multiple times for the same calculations.")
                if len(input[i]) == 3:
                    if input[i][0] == input[j][0] and input[i][2] == input[j][2]:
                        print_error("'" + input[i][0] + "' is defined multiple times for the same calculations.")
                if len(input[i]) == 4:
                    if input[i][0] == input[j][0] and input[i][2] == input[j][2] and input[i][3] == input[j][3]:
                        print_error("'" + input[i][0] + "' is defined multiple times for the same calculations.")
                    elif input[i][0] == input[j][0] and input[i][2] == input[j][3] and input[i][3] == input[j][2]:
                        print_error("'" + input[i][0] + "' is defined multiple times for the same calculations.")

    # You can't specify low/hi for the multimer_cutoff
    for entry in input:
        if entry[0] == "multimer_cutoff":
            if "lo" in entry or "hi" in entry:
                print_error("'multimer_cutoff' cannot be set to different values between the low and high level of the calculation")

    # You can't apply settings for multimers higher than the specified multimer_order
    for entry in input:
        if entry[0] == "multimer_order":
            multimer_order = entry[1]
    for entry in input:
        if multimer_order < 3:
            if "tri" in entry:
                print_error("Input file contains settings for multimers of higher order than the specified multimer order.")
            if multimer_order < 2:
                if "dim" in entry:
                    print_error("Input file contains settings for multimers of higher order than the specified multimer order.")



def convert_input_to_settings_dict(input):
    membed_keys = ["multimer_order",
                   "atoms_per_molecule",
                   "calculation_type",
                   "geometry_file_name",
                   "identify_fragments",
                   "symmetrize",
                   "f_max",
                   "compute_forces",
                   "compute_stress",
                   "vib_sc_size",
                   "vib_q_grid",
                   "vib_disp"]
    settings = {"membed": {},
                "per_lo": {"level": "low",  "system": "periodic", "directory": "calc", "vib_directory": "vib/"},
                "mon_lo": {"level": "low",  "system": "monomer",  "directory": "calc", "vib_directory": "vib/multimers_displaced"},
                "mon_hi": {"level": "high", "system": "monomer",  "directory": "calc", "vib_directory": "vib/multimers_displaced"},
                "dim_lo": {"level": "low",  "system": "dimer",    "directory": "calc", "vib_directory": "vib/multimers_displaced"},
                "dim_hi": {"level": "high", "system": "dimer",    "directory": "calc", "vib_directory": "vib/multimers_displaced"},
                "tri_lo": {"level": "low",  "system": "trimer",   "directory": "calc", "vib_directory": "vib/multimers_displaced"},
                "tri_hi": {"level": "high", "system": "trimer",   "directory": "calc", "vib_directory": "vib/multimers_displaced"}}


    # Search input for the MEmbed-specific keywords
    for entry in input:
        if entry[0] in membed_keys:
            settings["membed"][entry[0]] = entry[1]

    # Put all non-MEmbed-specific keywords in their respective dictionaries
    # Seperate for-loops ensure that specific keywords are prioritized over general keywords
    for item in settings.items():
        if not item[0] == "membed":
            for entry in input:
                if (not entry[0] in membed_keys) and len(entry) == 2:
                    settings[item[0]][entry[0]] = entry[1]
            for entry in input:
                if (not entry[0] in membed_keys) and len(entry) == 3:
                    if entry[2] in item[0]:
                        settings[item[0]][entry[0]] = entry[1]
            for entry in input:
                if (not entry[0] in membed_keys) and len(entry) == 4:
                    if entry[2] in item[0] and entry[3] in item[0]:
                        settings[item[0]][entry[0]] = entry[1]

    if settings["membed"]["calculation_type"] == "phonon":
        settings["membed"]["calculation_type"] = "vib"

    # Check if every calculation type has a calculator
    calculator_missing = None
    if not "calculator" in settings["per_lo"]:
        calculator_missing = "per_lo"
    elif not "calculator" in settings["mon_lo"]:
        calculator_missing = "mon_lo"
    elif not "calculator" in settings["mon_hi"]:
        calculator_missing = "mon_hi"

    elif settings["membed"]["multimer_order"] > 1:
        if not "calculator" in settings["dim_lo"]:
            calculator_missing = "dim_lo"
        elif not "calculator" in settings["dim_hi"]:
            calculator_missing = "dim_hi"

        elif settings["membed"]["multimer_order"] > 2:
            if not "calculator" in settings["tri_lo"]:
                calculator_missing = "tri_lo"
            elif not "calculator" in settings["tri_hi"]:
                calculator_missing = "tri_hi"

    if not calculator_missing == None:
        print_error("No calculator defined for calculation type '" + calculator_missing + "'.")

    # Check if dimers/trimers/etc. have a multimer cutoff (if they are to be calculated)
    multimer_cutoff_missing = None
    if settings["membed"]["multimer_order"] > 1:
        if not "multimer_cutoff" in settings["dim_hi"]:
            multimer_cutoff_missing = "dimers"

        elif settings["membed"]["multimer_order"] > 2:
            if not "multimer_cutoff" in settings["tri_hi"]:
                multimer_cutoff_missing = "trimers"

    if not multimer_cutoff_missing == None:
        print_error("No multimer cutoff defined for " + multimer_cutoff_missing + ".")

    # Multimer cutoff for dimers cannot be lower than multimer cutoff for trimers
    if settings["dim_hi"]["multimer_cutoff"] < settings["tri_hi"]["multimer_cutoff"]:
        print_error("Multimer cutoff for trimers cannot be larger than multimer cutoff for dimers.")

    # Set default vaules for calculate_forces and calculate_stress if they are not set
    if "compute_forces" not in settings["membed"]:
        if settings["membed"]["calculation_type"] == "sp":
            settings["membed"]["compute_forces"] = False
        elif settings["membed"]["calculation_type"] == "opt_int":
            settings["membed"]["compute_forces"] = True
        elif settings["membed"]["calculation_type"] == "opt_full" or settings["membed"]["calculation_type"] == "opt":
            settings["membed"]["compute_forces"] = True
        elif settings["membed"]["calculation_type"] == "vib":
            settings["membed"]["compute_forces"] = True
    if "compute_stress" not in settings["membed"]:
        if settings["membed"]["calculation_type"] == "sp":
            settings["membed"]["compute_stress"] = False
        elif settings["membed"]["calculation_type"] == "opt_int":
            settings["membed"]["compute_stress"] = False
        elif settings["membed"]["calculation_type"] == "opt_full" or settings["membed"]["calculation_type"] == "opt":
            settings["membed"]["compute_stress"] = True
        elif settings["membed"]["calculation_type"] == "vib":
            settings["membed"]["compute_stress"] = False

    # Check if forces/stress are turned on for optimzations or vibrations
    if settings["membed"]["calculation_type"] == "opt_int":
        if settings["membed"]["compute_forces"] == False:
            print_error("Calculation of analytical forces cannot be turned off for internal optimizations.")
    elif settings["membed"]["calculation_type"] == "opt_full" or settings["membed"]["calculation_type"] == "opt":
        if settings["membed"]["compute_forces"] == False:
            print_error("Calculation of analytical forces cannot be turned off for optimizations.")
        if settings["membed"]["compute_stress"] == False:
            print_error("Calculation of analytical stress cannot be turned off for optimizations.")
    elif settings["membed"]["calculation_type"] == "vib":
        if settings["membed"]["compute_forces"] == False:
            print_error("Calculation of analytical forces cannot be turned off for internal optimizations.")

    # Forbid calculation of forces and stress if trimers are included (not implemented yet)
    if settings["membed"]["multimer_order"] > 2:
        if settings["membed"]["compute_forces"]:
            print_error("The calculation of analytical forces is not yet implemented for a multimer order of 3.")

    # f_max cannot be set for single points or vibrations but has to be set for optimizations
    if settings["membed"]["calculation_type"] == "sp" or settings["membed"]["calculation_type"] == "vib":
        if "f_max" in settings["membed"]:
            print_error("'f_max' can only be set for optimizations.")
    else:
        if "f_max" not in settings["membed"]:
            print_error("'f_max' has to be set for optimizations.")

    # vib_sc_size vib_disp, and vib_q_grid can only be set for vibrations, also check if vib_sc_size has the right format and set vib_disp to default value if not given
    if settings["membed"]["calculation_type"] == "vib":
        vib_disp_default = 0.005
        if "vib_sc_size" not in settings["membed"]:
            print_error("'vib_sc_size' has to be set for vibrational calculations.")
        else:
            vib_sc_size = settings["membed"]["vib_sc_size"].split()
            if len(vib_sc_size) != 3 or not all(x.isdigit() for x in vib_sc_size):
                print_error("'vib_sc_size' cannot be interpreted properly. The correct syntax is 'a b c', where a, b, and c are the number of unit cells in the x, y, and z direction.")
        if "vib_disp" not in settings["membed"]:
            settings["membed"]["vib_disp"] = vib_disp_default
    else:
        if "vib_sc_size" in settings["membed"]:
            print_error("'vib_sc_size' can only be set for vibrational calculations.")
        if "vib_disp" in settings["membed"]:
            print_error("'vib_disp' can only be set for vibrational calculations.")
        if not settings["membed"]["vib_q_grid"] == "none":
            print_error("'vib_q_grid' can only be set for vibrational calculations.")

    # Check if vib_q_grid has the right format, if it is defined
    if not settings["membed"]["vib_q_grid"] == "none":
        vib_q_grid = settings["membed"]["vib_q_grid"].split()
        if len(vib_q_grid) != 3 or not all(x.isdigit() for x in vib_q_grid):
            print_error("'vib_q_grid' cannot be interpreted properly. The correct syntax is 'a b c', where a, b, and c are the number of q-points in the x, y, and z direction. "
                          + "Alternatively, it can be set to 'None'.")

    # Remove dictionaries of settings of higher multimer order than specified
    if settings["membed"]["multimer_order"] < 3:
        settings.pop("tri_lo")
        settings.pop("tri_hi")
        if settings["membed"]["multimer_order"] < 2:
            settings.pop("dim_lo")
            settings.pop("dim_hi")
            if settings["membed"]["multimer_order"] < 1:
                 settings.pop("mon_lo")
                 settings.pop("mon_hi")

    # Remove some partial settings that are not needed
    if not settings["membed"]["calculation_type"] == "vib":
        settings["membed"].pop("vib_q_grid")
    for group in ["per_lo", "mon_lo", "mon_hi"]:
        settings[group].pop("multimer_cutoff")

    # Print settings
    settings_out = deepcopy(settings)
    for group in ["per_lo", "mon_lo", "mon_hi", "dim_lo", "dim_hi", "tri_lo", "tri_hi"]:
        if group in settings_out:
            settings_out[group].pop("level")
            settings_out[group].pop("system")
            settings_out[group].pop("directory")
            settings_out[group].pop("vib_directory")

    dict_print = {"membed": "Global MEmbed settings:", "per_lo": "Periodic settings:",
                  "mon_lo": "Low monomer settings:", "mon_hi": "High monomer settings:",
                  "dim_lo": "Low dimer settings:", "dim_hi": "High dimer settings:",
                  "tri_lo": "Low trimer settings:", "tri_hi": "High trimer settings:"}

    for item in dict_print.items():
        if item[0] in settings_out:
            print("--------------------------------------------------------------------------------")
            print("  " + item[1])
            longest = max(map(len, settings_out[item[0]]))
            for jtem in settings_out[item[0]].items():
                print("    " + str(jtem[0]).ljust(longest+4) + str(jtem[1]))
    print("--------------------------------------------------------------------------------\n")

    return settings


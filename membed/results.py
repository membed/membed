#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from ase.db import connect
import numpy as np
import re
from ase import Atoms, Atom
from ase.spacegroup.symmetrize import symmetrize_rank1, symmetrize_rank2, prep_symmetry
from copy import deepcopy


def calculate_total_energy():
    """Combine the low-level periodic energy and all multimer energies into
    the approximated high-level periodic energy
    """

    db = connect("membed.db")
    monomers = list(db.select("mon_num"))
    dimers = list(db.select("dim_num"))
    trimers = list(db.select("tri_num"))
    multimer_types = list(db.select(calculate="F"))

    crystalrow = db.get(name="crystal")
    n_atoms = crystalrow.get("natoms")
    n_molecules = crystalrow.get("nmolecules")
    e_tot_low = crystalrow.get("low_energy")
    calc_type = crystalrow.data.settings["membed"]["calculation_type"]
    e_corr = 0.0

    # Calculate monomer correction energy
    e_corr_mon = 0.0
    for row_mon in monomers:
        if int(row_mon.mon_num) < n_molecules + 1:
            e_corr_mon += row_mon.get("high_energy") - row_mon.get("low_energy")

    # Calculate dimer correction energy
    e_corr_dim = 0.0
    for row_dim in dimers:
        for ref in multimer_types:
            if row_dim.get("mon_a_type") == ref.get("mon_type"):
                mon_a = ref
            if row_dim.get("mon_b_type") == ref.get("mon_type"):
                mon_b = ref

        e_dim_int_high = (row_dim.get("high_energy") -
                              mon_a.get("high_energy") -
                              mon_b.get("high_energy"))
        e_dim_int_low = (row_dim.get("low_energy") -
                              mon_a.get("low_energy") -
                              mon_b.get("low_energy"))
        # Take the full dimer interaction energy (high-low) for dimers fully inside central unit cell
        # Half of it if one monomer inside central unit cell
        in_unit_cell = 0.0
        if int(row_dim.mon_a_name[3:]) <= n_molecules:
            in_unit_cell += 0.5
        if int(row_dim.mon_b_name[3:]) <= n_molecules:
            in_unit_cell += 0.5
        e_corr_dim += (e_dim_int_high - e_dim_int_low) * in_unit_cell

    # Calculate trimer correction energy
    e_corr_tri = 0.0
    for row_tri in trimers:
        for ref in multimer_types:
            if row_tri.get("mon_a_type") == ref.get("mon_type"):
                mon_a = ref
            if row_tri.get("mon_b_type") == ref.get("mon_type"):
                mon_b = ref
            if row_tri.get("mon_c_type") == ref.get("mon_type"):
                mon_c = ref
            if row_tri.get("dim_ab_type") == ref.get("dim_type"):
                dim_ab = ref
            if row_tri.get("dim_ac_type") == ref.get("dim_type"):
                dim_ac = ref
            if row_tri.get("dim_bc_type") == ref.get("dim_type"):
                dim_bc = ref

        e_tri_int_high = (row_tri.get("high_energy") +
                              mon_a.get("high_energy") +
                              mon_b.get("high_energy") +
                              mon_c.get("high_energy") -
                              dim_ab.get("high_energy") -
                              dim_ac.get("high_energy") -
                              dim_bc.get("high_energy"))
        e_tri_int_low = (row_tri.get("low_energy") +
                              mon_a.get("low_energy") +
                              mon_b.get("low_energy") +
                              mon_c.get("low_energy") -
                              dim_ab.get("low_energy") -
                              dim_ac.get("low_energy") -
                              dim_bc.get("low_energy"))
        # Take the full trimer interaction energy (high-low) for trimers fully inside central unit cell
        # 2/3 of it if two monomers inside central unit cell
        # 1/3 of it if one monomer inside central unit cell
        in_unit_cell = 0.0
        if int(row_tri.mon_a_name[3:]) <= n_molecules:
            in_unit_cell += 1/3
        if int(row_tri.mon_b_name[3:]) <= n_molecules:
            in_unit_cell += 1/3
        if int(row_tri.mon_c_name[3:]) <= n_molecules:
            in_unit_cell += 1/3
        e_corr_tri += (e_tri_int_high - e_tri_int_low) * in_unit_cell


    # Sum up all correction energies
    e_corr = e_corr_mon + e_corr_dim + e_corr_tri
    e_tot_high = e_tot_low + e_corr

    if calc_type == "opt" or calc_type == "opt_full" or calc_type == "opt_int":
        add_to_sentence = " at this iteration"
        shift = 38
    else:
        add_to_sentence = ""
        shift = 20

    print("  Low-level periodic energy: " + str('{0:.10f}'.format(round(e_tot_low,10))).rjust(shift) + " eV")
    if len(monomers) > 0:
        print("  Monomer correction term:   " + str('{0:.10f}'.format(round(e_corr_mon,10))).rjust(shift) + " eV")
    if len(dimers) > 0:
        print("  Dimer correction term:     " + str('{0:.10f}'.format(round(e_corr_dim,10))).rjust(shift) + " eV")
    if len(trimers) > 0:
        print("  Trimer correction term:    " + str('{0:.10f}'.format(round(e_corr_tri,10))).rjust(shift) + " eV")
    print("\n  Total embedding energy" + add_to_sentence + ":    " + str('{0:.10f}'.format(round(e_tot_high,10))).rjust(20) + " eV")

    db.update(db.get(name="crystal").id, high_energy=e_tot_high)



def calculate_total_forces():
    """Combine the low-level periodic forces and all multimer forces into
    the approximated high-level periodic forces

    Currently implemented up to dimers
    """

    db = connect("membed.db")
    f_tot_low = db.get(name="crystal").data.low_forces
    f_corr = np.zeros((len(f_tot_low),3), dtype=float)
    n_molecules = db.get(name="crystal").nmolecules

    # Calculate monomer correction forces
    f_corr_mon = np.zeros((len(f_tot_low),3), dtype=float)
    for row_mon in db.select("mon_num"):
        indices = row_mon.data.indices
        for i in range(len(indices)):
            # Only add the forces of those atoms which lie in the central unit cell
            if indices[i] >= 0 and indices[i] < len(f_tot_low):
                f_corr_mon[indices[i]] = f_corr_mon[indices[i]] + row_mon.data.high_forces[i] - row_mon.data.low_forces[i]

    # Calculate dimer correction forces
    # f_corr_dim_str is needed for the stress calculation (forces of dimers which are only partially inside the unit cell are halved there)
    f_corr_dim = np.zeros((len(f_tot_low),3), dtype=float)
    f_corr_dim_str = np.zeros((len(f_tot_low),3), dtype=float)
    for row_dim in db.select("dim_num"):
        indices = row_dim.data.indices
        if int(row_dim.mon_a_name[3:]) <= n_molecules:
            if int(row_dim.mon_b_name[3:]) > n_molecules:
                factor = 2
            else:
                factor = 1

            for i in range(len(indices)):
                # Only add the forces of those atoms which lie in the central unit cell
                if indices[i] >= 0 and indices[i] < len(f_tot_low):
                    f_corr_dim[indices[i]] = f_corr_dim[indices[i]] + (row_dim.data.high_forces[i] - row_dim.data.low_forces[i] - f_corr_mon[indices[i]])
                    f_corr_dim_str[indices[i]] = f_corr_dim_str[indices[i]] + (row_dim.data.high_forces[i] - row_dim.data.low_forces[i] - f_corr_mon[indices[i]])/factor

    # Sum up all correction forces
    f_corr = f_corr_mon + f_corr_dim
    f_tot_high = f_tot_low + f_corr

    print("\n  Total embedding forces (eV/A):")
    print(re.sub('[\[\]]', ' ', np.array_str(f_tot_high)), flush = True)
    print("")

    db.update(db.get(name="crystal").id, data={"high_forces": f_tot_high, "f_corr_mon": f_corr_mon, "f_corr_dim_str": f_corr_dim_str})

    if db.get(name="crystal").data.settings["membed"]["symmetrize"]:
        symatoms = db.get(name="crystal").toatoms()
        srot, strans, symm_map = prep_symmetry(symatoms,symprec=1e-6,verbose=True)
        f_tot_high_sym = symmetrize_rank1(symatoms.get_cell(),
                                          symatoms.cell.reciprocal().T, f_tot_high,
                                          srot, strans, symm_map)

        print("\n  Symmetrized total embedding forces (eV/A):")
        print(re.sub('[\[\]]', ' ', np.array_str(f_tot_high_sym)), flush = True)

        db.update(db.get(name="crystal").id, data={"high_forces_sym": f_tot_high_sym})


def calculate_total_stress():
    """Combine the low-level periodic stress and the multimer forces into
    the approximated high-level periodic stress
    """

    db = connect("membed.db")
    crystal = db.get(name="crystal")

    # Get needed values from database
    stress_low = crystal.data.low_stress
    positions = crystal.toatoms().get_positions()
    volume = crystal.toatoms().cell.volume
    n_molecules = crystal.nmolecules

    # Get monomer and dimer force corrections from inside of the unit cell
    # (already calculated by calculate_total_forces)
    f_corr_mon = crystal.data.f_corr_mon
    f_corr_dim_inside = crystal.data.f_corr_dim_str

    # Get dimers from database
    dimers = []
    for row_dim in db.select("dim_num"):
        dimers.append(row_dim)

    # Monomer contribution to the stress:
    s_corr_mon = np.zeros((3,3), float)
    for i in range(3):
        for j in range(3):
            for k in range(len(f_corr_mon)):
                s_corr_mon[i][j] += f_corr_mon[k][j] * positions[k][i]

    # Dimer contribution to the stress:

    # Dimers and dimer parts inside of unit cell
    s_corr_dim_inside = np.zeros((3,3), float)
    for i in range(3):
        for j in range(3):
            for k in range(len(f_corr_dim_inside)):
                s_corr_dim_inside[i][j] += f_corr_dim_inside[k][j] * positions[k][i]

    # Dimer parts outside of unit cell
    s_corr_dim_outside = np.zeros((3,3), float)
    for idx in range(len(dimers)):
        mon_a_name = dimers[idx].mon_a_name
        mon_b_name = dimers[idx].mon_b_name
        if int(mon_a_name[3:]) <= n_molecules and int(mon_b_name[3:]) > n_molecules: # Only include dimers with one monomer outside of the unit cell
            mon_b = db.get(name=mon_b_name)
            mon_b_monforce_high = mon_b.data.high_forces
            mon_b_monforce_low = mon_b.data.low_forces
            mon_b_indices = mon_b.data.indices

            # Find the atoms of the dimer that belong to monomer b and write their forces to mon_b_dimforce(_high/_low)
            dimer_indices = dimers[idx].data.indices
            dimer_highforce = dimers[idx].data.high_forces
            dimer_lowforce = dimers[idx].data.low_forces
            mon_b_dimforce_high = np.zeros((len(mon_b_monforce_high),3), float)
            mon_b_dimforce_low = np.zeros((len(mon_b_monforce_low),3), float)
            for i in range(len(mon_b_indices)):
                for j in range(len(dimer_indices)):
                    if mon_b_indices[i] == dimer_indices[j]:
                        mon_b_dimforce_high[i] = dimer_highforce[j]
                        mon_b_dimforce_low[i] = dimer_lowforce[j]

            # Subtract the monomer forces from the dimer forces
            mon_b_dimer_interaction_force_high = mon_b_dimforce_high - mon_b_monforce_high
            mon_b_dimer_interaction_force_low = mon_b_dimforce_low - mon_b_monforce_low

            # Build difference of high and low forces
            mon_b_dimer_interaction_force_emb = mon_b_dimer_interaction_force_high - mon_b_dimer_interaction_force_low

            mon_b_positions = mon_b.toatoms().get_positions()

            for i in range(3):
                for j in range(3):
                    for k in range(len(mon_b_positions)):
                        s_corr_dim_outside[i][j] += mon_b_dimer_interaction_force_emb[k][j]/2 * mon_b_positions[k][i]

    # Sum up inside and outside dimer contributions
    s_corr_dim = s_corr_dim_inside + s_corr_dim_outside

    # Sum up the stress contributions
    stress_emb = -(s_corr_mon + s_corr_dim) / volume
    stress_high = stress_low + stress_emb

    db.update(db.get(name="crystal").id, data={"high_stress": stress_high})

    print("\n  Total embedding stress (eV/A**3): ")
    print(re.sub('[\[\]]', ' ', np.array_str(stress_high)), flush = True)

    if crystal.data.settings["membed"]["symmetrize"]:
        symatoms = crystal.toatoms()
        srot, strans, symm_map = prep_symmetry(symatoms,symprec=1e-6,verbose=False)
        stress_high_sym = symmetrize_rank2(symatoms.get_cell(),
                                           symatoms.cell.reciprocal().T, stress_high,
                                           srot)

        db.update(db.get(name="crystal").id, data={"high_stress_sym": stress_high_sym})

        print("\n  Symmetrized total embedding stress (eV/A**3): ")
        print(re.sub('[\[\]]', ' ', np.array_str(stress_high_sym)), flush = True)
        print("")


def calculate_total_forcesets(sc_size):
    """Combine the low-level periodic forces of the displaced supercells and the multimer forces
    into the approximated high-level forces.

    Effectively, for each unit cell of the supercell, the embedding force correction of the non-displaced structure is added,
    but then force components are replaced by those of displaced multimers.

    Currently implemented up to dimers.
    """

    # Load everything needed from database
    db = connect("membed.db")
    crystalrow = db.get(name="crystal")
    displacements = list(db.select("displacement_num"))
    disp_monomers = list(db.select("disp_mon_num"))
    disp_dimers = list(db.select("disp_dim_num"))
    monomers = list(db.select("mon_num"))
    dimers = list(db.select("dim_num"))

    # Get some needed values
    emb_forces = crystalrow.data.high_forces - crystalrow.data.low_forces
    n_atoms = crystalrow.natoms
    nr_of_unit_cells_phonopy = sc_size[0] * sc_size[1] * sc_size[2]
    nr_of_unit_cells_membed = crystalrow.data.sc_dimensions[0] * crystalrow.data.sc_dimensions[1] * crystalrow.data.sc_dimensions[2]

    # Calculate high forces for each of the displaced supercells
    low_force_sets = []
    high_force_sets = []
    for i in range(len(displacements)):
        id = displacements[i].displacement_num
        low_forces = displacements[i].data.low_forces

        # Monomer correction
        f_corr_mon = np.zeros((len(low_forces),3), dtype=float)
        for row in disp_monomers:
            if row.disp_id == id:
                orig_mon = next((x for x in monomers if x.name == row.orig_monomer))
                indices = row.data.indices
                for j in range(len(indices)):
                    if indices[j] >= 0 and indices[j] < n_atoms:
                        f_corr_mon[indices[j] * nr_of_unit_cells_phonopy] += (row.data.high_forces[j] - row.data.low_forces[j]
                                                                           - (orig_mon.data.high_forces[j] - orig_mon.data.low_forces[j]))

        # Dimer correction
        f_corr_dim = np.zeros((len(low_forces),3), dtype=float)
        for row in disp_dimers:
            if row.disp_id == id:
                orig_dim = next((x for x in dimers if x.name == row.orig_dimer))
                indices = row.data.indices

                # Find out to which unit cell of the supercell the monomer b forces have to be added
                # If the phonopy unit cell is too small, the forces have to be added to the unit cell which
                # is a repeating unit of the original unit cell...
                phonopy_mon_b_sc_sector = [None] * 3
                include_mon_b_forces = False
                for j in range(3):
                    if abs(row.data.mon_b_sc_sector[j]) + 1 <= sc_size[j]:
                        if row.data.mon_b_sc_sector[j] >= 0:
                            phonopy_mon_b_sc_sector[j] = 1 + row.data.mon_b_sc_sector[j]
                        else:
                            phonopy_mon_b_sc_sector[j] = 1 + sc_size[j] + row.data.mon_b_sc_sector[j]
                    else:
                        if abs(row.data.mon_b_sc_sector[j]) % sc_size[j] == 0:
                            phonopy_mon_b_sc_sector[j] = 1
                        else:
                            if row.data.mon_b_sc_sector[j] >= 0:
                                phonopy_mon_b_sc_sector[j] = 1 + row.data.mon_b_sc_sector[j] % sc_size[j]
                            else:
                                phonopy_mon_b_sc_sector[j] = 1 + sc_size[j] - abs(row.data.mon_b_sc_sector[j]) % (sc_size+1)

                    # ...as long as this repeating unit is not the one where the displacement occurs (supercell-sector 1 1 1)
                    if phonopy_mon_b_sc_sector[j] != 1:
                        include_mon_b_forces = True

                # Calculate the number that has to be added to the index in order to end up in the right unit cell
                uc_pointer = (phonopy_mon_b_sc_sector[0] - 1) + sc_size[0] * (phonopy_mon_b_sc_sector[1] - 1) + sc_size[0] * sc_size[1] * (phonopy_mon_b_sc_sector[2] - 1)

                for j in range(len(indices)):
                    if indices[j] >= 0 and indices[j] < n_atoms:
                        f_corr_dim[indices[j] * nr_of_unit_cells_phonopy] += (row.data.high_forces[j] - row.data.low_forces[j]
                                                                           - (orig_dim.data.high_forces[j] - orig_dim.data.low_forces[j])
                                                                           - f_corr_mon[indices[j] * nr_of_unit_cells_phonopy])

                    elif include_mon_b_forces:
                        projected_atom_idx = (indices[j] + (nr_of_unit_cells_membed // 2) * n_atoms) % n_atoms
                        f_corr_dim[projected_atom_idx * nr_of_unit_cells_phonopy + uc_pointer] += (row.data.high_forces[j] - row.data.low_forces[j]
                                                                                                - (orig_dim.data.high_forces[j] - orig_dim.data.low_forces[j])
                                                                                                - f_corr_mon[projected_atom_idx * nr_of_unit_cells_phonopy + uc_pointer])


        # Combine all forces
        f_corr_tot = f_corr_mon + f_corr_dim
        high_forces = np.zeros((len(low_forces),3), float)
        high_forces = low_forces + f_corr_tot
        for j in range(len(high_forces)):
            high_forces[j] += emb_forces[j // nr_of_unit_cells_phonopy]

        low_force_sets.append(low_forces)
        high_force_sets.append(high_forces)

    # Save forces to database
    with db:
        db.update(crystalrow.id, data={"low_force_sets": low_force_sets, "high_force_sets": high_force_sets})
        for i in range(len(displacements)):
            db.update(displacements[i].id, data={"high_forces": high_force_sets[i]})





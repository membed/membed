#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from copy import deepcopy
from ase.io import write
from ase.db import connect
from ase import Atoms, Atom
from ase.io import read
from ase.calculators.calculator import FileIOCalculator, Parameters, ReadError, PropertyNotImplementedError
import os
from .calculate import run_calculations
from .cell import unwrap, get_supercell, assign_atoms_to_fragments
from .database import save_to_database, synchronize_database
from .multimers import get_monomers, get_dimers, get_trimers, get_duplicates
from .results import calculate_total_energy, calculate_total_forces, calculate_total_stress
from .output import print_geometry


class MEmbed(FileIOCalculator):

    #__command_default = ''
    #__outfilename_default = ''

    implemented_properties = ['energy', 'free_energy', 'forces', 'stress']
    command = ''
    discard_results_on_any_change = True

    def __init__(self, restart=None,
                 label=os.curdir, atoms=None, kpts=None, command=None,
                 outfilename=None, settings=None, **kwargs):
        """
        MEmbed Calculator

        #INFO: Free_energy is currently identical to energy.
        """
        FileIOCalculator.__init__(self, restart=restart,
                                  label=label, atoms=atoms, command=command, **kwargs)
        self.calc = None
        self.settings = settings

    #def calculation_required(self, atoms, quantities):
    #    return True

    def write_input(self, atoms, properties, system_changes):
        FileIOCalculator.write_input(self, atoms, properties, system_changes)

    def calculate(self, atoms, properties, system_changes):
        membed_main(atoms, self.settings)
        FileIOCalculator.calculate(self, atoms, properties, system_changes)

    def read_results(self):
        db = connect("membed.db")
        energy = db.get(name="crystal").high_energy
        self.results['energy'] = energy
        self.results['free_energy'] = energy
        if self.settings["membed"]["compute_forces"] or self.settings["membed"]["compute_stress"]:
            forces = db.get(name="crystal").data.high_forces
            self.results['forces'] = forces
            if self.settings["membed"]["compute_stress"]:
                stress = db.get(name="crystal").data.high_stress
                self.results['stress'] = stress


def membed_main(atoms_orig, settings):
    """Main routine of the MEmbed calculator"""

    # Clean up from previous calculations
    if os.path.exists("membed.db"):
        os.remove("membed.db")
    if os.path.isdir("calc/"):
        for calc_file in os.listdir("calc/"):
            if os.path.isfile("calc/" + calc_file) and calc_file.endswith(".out"):
                os.remove("calc/" + calc_file)

    atoms = deepcopy(atoms_orig)

    # Create unwrapped structure
    atoms = unwrap(atoms)
    write('current_geometry.in', atoms, format='aims')

    # Write structure to output if optimization is performed
    if settings["membed"]["calculation_type"] == "opt" or settings["membed"]["calculation_type"] == "opt_full" or settings["membed"]["calculation_type"] == "opt_int":
        print("\n--------------------------------------------------------------------------------\n", flush=True)
        print("  Current geometry:")
        print_geometry(atoms)

    # Generate supercell and identify which atoms belong to which molecule
    if settings["membed"]["multimer_order"] > 1:
        supercell, sc_dimensions = get_supercell(atoms, settings["dim_hi"]["multimer_cutoff"])
    else:
        supercell, sc_dimensions = get_supercell(atoms, 0)
    supercell = assign_atoms_to_fragments(supercell, settings["membed"]["fragments"], len(atoms))

    # Save crystal structure to database, together with settings and other information
    n_molecules = int(len(atoms_orig) / settings["membed"]["atoms_per_molecule"])
    atoms.n_molecules = n_molecules
    atoms.settings = settings
    atoms.sc_dimensions = sc_dimensions
    save_to_database(atoms=atoms)

    # Generate multimers up to the specified multimer order
    # The multimers from the whole supercell will only be generated if they are needed for the generation of higher multimers
    multimer_order = settings["membed"]["multimer_order"]
    # Monomers
    if multimer_order > 0:
        monomers = get_monomers(supercell, sc_dimensions, len(atoms), max(settings["membed"]["fragments"]), multimer_order > 1)
        monomers, mons_to_calc = get_duplicates(monomers)
        print("  Identified " + str(len(monomers)) + " monomers, " + str(mons_to_calc) + " of which to calculate.", flush=True)
        save_to_database(monomers=monomers)
        # Dimers
        if multimer_order > 1:
            dimers = get_dimers(monomers, settings["dim_hi"]["multimer_cutoff"], n_molecules, multimer_order > 2)
            dimers, dims_to_calc = get_duplicates(dimers)
            print("  Identified " + str(len(dimers)) + " dimers, " + str(dims_to_calc) + " of which to calculate.", flush=True)
            save_to_database(dimers=dimers)
            #Trimers
            if multimer_order > 2:
                trimers = get_trimers(monomers, dimers, settings["tri_hi"]["multimer_cutoff"], n_molecules, multimer_order > 3)
                trimers, tris_to_calc = get_duplicates(trimers)
                print("  Identified " + str(len(trimers)) + " trimers, " + str(tris_to_calc) + " of which to calculate.", flush=True, end="\n\n")
                save_to_database(trimers=trimers)

    # Perform all necessary calculations and copy the result to duplicate multimers
    run_calculations(settings)
    synchronize_database(settings["membed"]["compute_forces"])

    # Combine all calculation results into the approximate values for the high-level periodic calculation
    calculate_total_energy()
    if settings["membed"]["compute_forces"] or settings["membed"]["compute_stress"]:
        calculate_total_forces()
        if settings["membed"]["compute_stress"]:
            calculate_total_stress()
    print("\n--------------------------------------------------------------------------------\n")




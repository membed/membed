#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from ase import Atoms, Atom
from ase.db import connect
from ase.io import write, read
from ase.units import kJ, mol
from .database import save_displacements_to_database
from .calculate import run_calculations
from .results import calculate_total_forcesets
import os
import numpy as np
import time
from phonopy import Phonopy, load
from phonopy.structure.atoms import PhonopyAtoms
from phonopy.interface.calculator import read_crystal_structure
from phonopy.interface.aims import read_aims_output, write_aims

def vib_main(settings):
    """Main functing for handling the calculation of vibrations"""

    # Get relevant data from database
    db = connect("membed.db")
    crystalrow = db.get(name="crystal")
    n_atoms = crystalrow.natoms
    n_molecules = crystalrow.nmolecules
    atoms = crystalrow.toatoms()
    sc_size = [int(x) for x in settings["membed"]["vib_sc_size"].split()]
    disp_amp = settings["membed"]["vib_disp"]

    # Remove 'vib_' prefix from the settings
    for group in settings:
        if group != "membed":
            for setting in list(settings[group]):
                if setting.startswith("vib_"):
                    settings[group][setting[4:]] = settings[group][setting]
                    settings[group].pop(setting)

    # Create necessary displacements using phonopy and save displaced supercells to database
    displacements, displaced_supercells = create_displacements(atoms, sc_size, disp_amp, settings["per_lo"])
    save_displacements_to_database(supercells=displaced_supercells)

    # Create displaced multimers and save them to database
    displace_multimers(sc_size, displacements, n_atoms, n_molecules, settings["membed"]["multimer_order"])

    run_calculations(settings)

    # Calculate corrected forces for each displacement
    calculate_total_forcesets(sc_size)

    # Write an outfile for each displacement containing the corrected forces
    globals()["write_corrected_outfile_" + settings["per_lo"]["calculator"]]()

    calculate_and_save_vibrational_properties(settings["membed"]["vib_q_grid"])


def create_displacements(atoms, sc_size, disp_amp, settings):
    """Create file architecture and displaced supercells for subsequent calculation with FHI-aims"""

    # Create "vib" directory if it doesn't exist yet
    if not os.path.isdir("vib"):
        os.mkdir("vib")

    # Convert ASE Atoms object to Phonopy Atoms object
    write("vib/geometry.in", atoms)
    unitcell, _ = read_crystal_structure("vib/geometry.in", interface_mode="aims")

    # Create Phonopy object
    phonon = Phonopy(unitcell, supercell_matrix = [[sc_size[0], 0, 0],
                                                   [0, sc_size[1], 0],
                                                   [0, 0, sc_size[2]]])

    # Write supercell
    supercell = phonon.get_supercell()
    write_aims("vib/geometry.in.supercell", supercell)

    # Generate displacements
    phonon.generate_displacements(distance=disp_amp)

    # Create folder for each displacement, write displaced supercells to folders, and read supercells as ASE Atoms object
    disp_supercells = phonon.supercells_with_displacements
    displacements = phonon.get_displacements()
    disp_supercells_ase = []
    for i in range(len(disp_supercells)):
        if not os.path.isdir("vib/disp-" + str(i+1).zfill(3)):
            os.mkdir("vib/disp-" + str(i+1).zfill(3))
        write_aims("vib/disp-" + str(i+1).zfill(3) + "/geometry.in", disp_supercells[i])
        disp_supercells_ase.append(read("vib/disp-" + str(i+1).zfill(3) + "/geometry.in"))
        disp_supercells_ase[i].displacement = displacements[i]

    # Save parameters
    phonon.save(filename="vib/phonopy_disp.yaml")

    print("\n--------------------------------------------------------------------------------\n")
    print("  Generated " + str(len(displacements)) + " supercells containing finite displacements for the phonon calculation.")

    return displacements, disp_supercells_ase


def displace_multimers(sc_size, displacements, n_atoms, n_molecules, multimer_order):
    """Use the displacements created with phonopy to find out which multimers have to be displaced as well and save them to the database"""

    db = connect("membed.db")
    crystalrow = db.get(name="crystal")

    nr_of_unit_cells_phonopy = sc_size[0] * sc_size[1] * sc_size[2]
    nr_of_unit_cells_membed = crystalrow.data.sc_dimensions[0] * crystalrow.data.sc_dimensions[1] * crystalrow.data.sc_dimensions[2]

    # Get multimers from database
    monomers = []
    dimers = []
    for row in db.select("mon_num"):
        monomers.append(row)
    for row in db.select("dim_num"):
        dimers.append(row)

    displaced_monomers = []
    displaced_dimers = []

    # For each displacement, find all multimers that contain the atom that was displaced
    for i in range(len(displacements)):
        displaced_atom = displacements[i][0] / nr_of_unit_cells_phonopy

        # Monomers
        for row in monomers:
            if row.mon_num < n_molecules + 1:
                if displaced_atom in row.data.indices:
                    idx = row.data.indices.index(displaced_atom)
                    atoms = row.toatoms()
                    for j in range(3):
                        atoms[idx].position[j] += displacements[i][j+1]

                    # Write information worth saving to atoms object
                    atoms.orig_monomer = row.name
                    atoms.disp_id = i+1
                    atoms.displacement_magnitude = [displacements[i][1], displacements[i][2], displacements[i][3]]
                    atoms.displaced_atom = idx
                    atoms.indices = row.data.indices
                    displaced_monomers.append(atoms)

        # Dimers
        for row in dimers:
            if int(row.mon_a_name[3:]) < n_molecules + 1:
                if displaced_atom in row.data.indices:
                    idx = row.data.indices.index(displaced_atom)
                    atoms = row.toatoms()
                    for j in range(3):
                        atoms[idx].position[j] += displacements[i][j+1]

                    # Does the outside monomer of the dimer need to be displaced, too?
                    # First, check if the phonopy supercell is too small to fit monomer b normally
                    phonopy_sc_too_small = False
                    for j in range(3):
                        if abs(row.data.mon_b_sc_sector[j]) >= sc_size[j]:
                            phonopy_sc_too_small = True

                    # If that is given, check if monomer b lies in a unit cell that is a repeating unit of the unit cell in which the displacement happens
                    projects_onto_displaced_unit_cell = False
                    if phonopy_sc_too_small:
                        projects_onto_displaced_unit_cell = True
                        for j in range(3):
                            if abs(row.data.mon_b_sc_sector[j]) % sc_size[j] != 0:
                                projects_onto_displaced_unit_cell = False

                    # Finally, displace monomer b if it also is a repeating unit of monomer a
                    if projects_onto_displaced_unit_cell:
                        for j in range(len(row.data.indices)):
                            if not (row.data.indices[j] >= 0 and row.data.indices[j] < n_atoms):
                                projected_atom_idx = (row.data.indices[j] + (nr_of_unit_cells_membed // 2) * n_atoms) % n_atoms
                                if projected_atom_idx == displaced_atom:
                                    for k in range(3):
                                        atoms[j].position[k] += displacements[i][k+1]

                    # Write information worth saving to atoms object
                    atoms.orig_dimer = row.name
                    atoms.orig_mon_a = row.mon_a_name
                    atoms.orig_mon_b = row.mon_b_name
                    atoms.mon_b_sc_sector = row.data.mon_b_sc_sector
                    atoms.disp_id = i+1
                    atoms.displacement_magnitude = [displacements[i][1], displacements[i][2], displacements[i][3]]
                    atoms.displaced_atom = idx
                    atoms.indices = row.data.indices
                    displaced_dimers.append(atoms)

    if len(displaced_monomers) > 0:
        print("  Identified " + str(len(displaced_monomers)) + " displaced monomers that have to be calculated.")
    if len(displaced_dimers) > 0:
        print("  Identified " + str(len(displaced_dimers)) + " displaced dimers that have to be calculated.")

    # Save the displacements and the displaced multimers to the database
    save_displacements_to_database(monomers=displaced_monomers)
    if multimer_order > 1:
        save_displacements_to_database(dimers=displaced_dimers)


def write_corrected_outfile_aims():
    """Read the 'aims.out' file in each of the subfolders, replace the low-level total atomic forces
    with the high-level ones and write all lines needed by Phonopy to the 'aims_emb.out' file.
    Write collected forces to FORCE_SETS.
    """

    db = connect("membed.db")
    displacements = list(db.select("displacement_num"))

    with open("vib/FORCE_SETS", "w") as f:
        f.write(str(len(displacements[0].toatoms())).ljust(5) + "\n" + str(len(displacements)).ljust(5) + "\n")

    for disp in displacements:
        high_forces = disp.data.high_forces
        lines_in = open("vib/disp-" + str(disp.displacement_num).zfill(3) + "/displacement" + str(disp.displacement_num) + "_low.out","r").readlines()

        lines_out = []
        lines_out.append("# This file contains the lines from aims.out necessary for interpretation by Phonopy\n")
        lines_out.append("# Forces are replaced with the approximated high-level forces of the embedding calculation\n")
        lines_out.append("# Created using MEmbed\n")
        lines_out.append("# " + time.asctime() + "\n")

        for i in range(len(lines_in)):
            if "| Number of atoms" in lines_in[i]:
                N = int(lines_in[i].split()[5])
                lines_out.append(lines_in[i])

            elif "| Unit cell:" in lines_in[i]:
                for j in range(4):
                    lines_out.append(lines_in[i+j])

            elif ("Atomic structure:" in lines_in[i]) or ("Updated atomic structure:" in lines_in[i]):
                for j in range(N+2):
                    lines_out.append(lines_in[i+j])

            elif "Total atomic forces" in lines_in[i]:
                lines_out.append(lines_in[i])
                for j in range(N):
                    lines_out.append("  |" + str(j+1).rjust(5)
                                           + str('{0:.15f}'.format(round(high_forces[j][0],15))).rjust(27)
                                           + str('{0:.15f}'.format(round(high_forces[j][1],15))).rjust(27)
                                           + str('{0:.15f}'.format(round(high_forces[j][2],15))).rjust(27) + "\n")

        with open("vib/disp-" + str(disp.displacement_num).zfill(3) + "/aims_emb.out", "w") as f:
            for i in range(len(lines_out)):
                f.write(lines_out[i])

        with open("vib/FORCE_SETS", "a") as f:
            f.write("\n" + str(disp.displaced_atom + 1).ljust(5) + "\n")
            for i in range(3):
                    f.write(str('{0:.16f}'.format(round(disp.data.displacement[i+1],15))).rjust(20 + (i+1)//2))
            f.write("\n")
            for i in range(len(high_forces)):
                for j in range(3):
                    f.write(str('{0:.10f}'.format(round(high_forces[i][j],10))).rjust(15 + (j+1)//2))
                f.write("\n")


def calculate_and_save_vibrational_properties(q_grid):
    """Calculate gamma-frequencies and thermal properties (at gamma point and for the given q-grid) and save them to the database"""

    def get_thermal_properties(q_grid, E_el):
        phonon.run_mesh(q_grid)
        mesh_dict = phonon.get_mesh_dict()

        phonon.run_thermal_properties(t_step=10, t_max=300, t_min=0, cutoff_frequency=299792458/1e10)
        tp_dict = phonon.get_thermal_properties_dict()
        tp_dict['q_grid'] = q_grid

        tp_dict['temperatures_k'] = tp_dict.pop('temperatures')

        conv = mol / kJ

        tp_dict['vib_free_energy_kj/mol'] = tp_dict.pop('free_energy')
        tp_dict['vib_free_energy_ev'] = [entry / conv for entry in tp_dict['vib_free_energy_kj/mol']]

        tp_dict['vib_entropy_j/molk'] = tp_dict.pop('entropy')
        tp_dict['vib_entropy_mev/k'] = [entry / conv for entry in tp_dict['vib_entropy_j/molk']]

        tp_dict['heat_capacity_j/molk'] = tp_dict.pop('heat_capacity')
        tp_dict['heat_capacity_mev/k'] = [entry / conv for entry in tp_dict['heat_capacity_j/molk']]

        tp_dict['vib_energy_kj/mol'] = [F + S*T/1000 for F, S, T in zip(tp_dict['vib_free_energy_kj/mol'], tp_dict['vib_entropy_j/molk'], tp_dict['temperatures_k'])]
        tp_dict['vib_energy_ev'] = [F + S*T/1000 * S for F, S, T in zip(tp_dict['vib_free_energy_ev'], tp_dict['vib_entropy_mev/k'], tp_dict['temperatures_k'])]

        tp_dict['free_energy_kj/mol'] = [F + E_el*conv for F in tp_dict['vib_free_energy_kj/mol']]
        tp_dict['free_energy_ev'] = [F + E_el for F in tp_dict['vib_free_energy_ev']]

        return tp_dict, mesh_dict

    phonon = load("vib/phonopy_disp.yaml", force_sets_filename="vib/FORCE_SETS")
    force_sets_lines = open("vib/FORCE_SETS", "r").readlines()

    db = connect("membed.db")
    E_el = db.get(name="crystal").high_energy

    thermal_props_gamma, mesh_dict_gamma = get_thermal_properties([1, 1, 1], E_el)
    print("Gamma point frequencies (1/cm):")
    for freq in mesh_dict_gamma["frequencies"][0]:
        print(str('{0:.10f}'.format(round(freq * 1e10 / 299792458,10))).rjust(20))

    thermal_props = None
    if not q_grid == "none":
        thermal_props, mesh_dict = get_thermal_properties([int(x) for x in q_grid.split()], E_el)
        print("\n  Total embedding Helmholtz free energy at 0 K:                 " + str(thermal_props["free_energy_ev"][0]) + " eV")
        print("  Total embedding Helmholtz free energy at 300 K:               " + str(thermal_props["free_energy_ev"][30]) + " eV")
        print("  (Thermal properties were calculated using a " + q_grid + " q-grid.)")

    print("")
    print("  WARNING: Vibrational properties are currently calculated using all frequencies larger")
    print("           than 1 cm^-1 for the requested q-grid, irrespective of eventual numerical")
    print("           problems. Therefore, it is recommended to always check these results with an")
    print("           explicit phonopy calculation, which can be performed in the vib subdirectory!")
    print("")
    print("--------------------------------------------------------------------------------\n")


    db = connect("membed.db")
    db.update(db.get(name="crystal").id, data = {"high_force_sets_lines": force_sets_lines,
                                                 "thermal_properties_gamma": thermal_props_gamma,
                                                 "thermal_properties": thermal_props,
                                                 "mesh_dict_gamma": mesh_dict_gamma})


#!/usr/bin/env python
#
# MEmbed: Multimer Embedding Code for the Simulation of Molecular Crystals
# Code available at: https://gitlab.com/membed/membed
#
# Copyright (C): 2021-2022 Johannes Hoja
#                     2022 Alexander List
#                     2022 A. Daniel Boese
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from copy import deepcopy
from ase.db import connect
from ase import Atoms, Atom
import numpy as np


def save_to_database(atoms=None, monomers=None, dimers=None, trimers=None):
    """Save all generated multimer data to the membed.db database"""

    if not atoms == None:
        # Save atoms without constraints in database:
        db = connect("membed.db")
        dbatoms = deepcopy(atoms)
        dbatoms.set_constraint(None)
        db.write(dbatoms, name="crystal", nmolecules=atoms.n_molecules, data={"settings": atoms.settings, "sc_dimensions": atoms.sc_dimensions})
        return

    if not monomers == None:
        with connect("membed.db") as db:
            for i in range(len(monomers)):
                db.write(monomers[i],
                        name="mon%s" % str(i+1),
                        mon_num=i+1,
                        mon_type=monomers[i].multimer_type,
                        calculate=monomers[i].calculate,
                        data={"indices": monomers[i].indices, "rotation_matrix": monomers[i].rotation_matrix, "sc_sector": monomers[i].sc_sector})
        return
    
    if not dimers == None:
        with connect("membed.db") as db:
            for i in range(len(dimers)):
                db.write(dimers[i],
                        name="dim%s" % str(i+1),
                        dim_num=i+1,
                        dim_type=dimers[i].multimer_type,
                        mon_a_n=dimers[i].mon_a_n,
                        mon_a_type=dimers[i].mon_a_type,
                        mon_b_n=dimers[i].mon_b_n,
                        mon_b_type=dimers[i].mon_b_type,
                        calculate=dimers[i].calculate,
                        dim_dist=dimers[i].dim_dist,
                        mon_a_name=dimers[i].mon_a_name,
                        mon_b_name=dimers[i].mon_b_name,
                        data={"indices": dimers[i].indices, "rotation_matrix": dimers[i].rotation_matrix, "mon_a_sc_sector": dimers[i].mon_a_sc_sector, "mon_b_sc_sector": dimers[i].mon_b_sc_sector})
        return

    if not trimers == None:
        with connect("membed.db") as db:
            for i in range(len(trimers)):
                db.write(trimers[i],
                        name="tri%s" % str(i+1),
                        tri_num=i+1,
                        tri_type=trimers[i].multimer_type,
                        mon_a_n=trimers[i].mon_a_n,
                        mon_a_type=trimers[i].mon_a_type,
                        mon_a_name=trimers[i].mon_a_name,
                        mon_b_n=trimers[i].mon_b_n,
                        mon_b_type=trimers[i].mon_b_type,
                        mon_b_name=trimers[i].mon_b_name,
                        mon_c_n=trimers[i].mon_c_n,
                        mon_c_type=trimers[i].mon_c_type,
                        mon_c_name=trimers[i].mon_c_name,
                        calculate=trimers[i].calculate,
                        dim_ab_name = trimers[i].dim_ab_name,
                        dim_ab_type = trimers[i].dim_ab_type,
                        dim_ac_name = trimers[i].dim_ac_name,
                        dim_ac_type = trimers[i].dim_ac_type,
                        dim_bc_name = trimers[i].dim_bc_name,
                        dim_bc_type = trimers[i].dim_bc_type,
                        dim_dist_ab = trimers[i].dim_dists[0],
                        dim_dist_ac = trimers[i].dim_dists[1],
                        dim_dist_bc = trimers[i].dim_dists[2],
                        data={"indices": trimers[i].indices, "rotation_matrix": trimers[i].rotation_matrix,
                              "mon_a_sc_sector": trimers[i].mon_a_sc_sector, "mon_b_sc_sector": trimers[i].mon_b_sc_sector, "mon_c_sc_sector": trimers[i].mon_c_sc_sector})
        return


def update_database(name, content):
    """Write energies and forces to the database

    If an entry has both high and low energy, set their calculation status to finished
    """

    db = connect("membed.db")
    id = db.get(name=name).id
    for i in range(len(content)):
        if content[i][0] == "high_energy":
            db.update(id, high_energy=content[i][1])
        if content[i][0] == "low_energy":
            db.update(id, low_energy=content[i][1])
        if content[i][0] == "high_forces":
            db.update(id, data={"high_forces": content[i][1]})
        if content[i][0] == "low_forces":
            db.update(id, data={"low_forces": content[i][1]})
        if content[i][0] == "low_stress":
            db.update(id, data={"low_stress": content[i][1]})

        if db.get(name=name).get("high_energy") != None and db.get(name=name).get("low_energy") != None:
            db.update(id, calculate="F")


def synchronize_database(forces):
    """Copy calculated values to the identical multimers

    If forces were calculated, use rotation matrix to rotate the forces so they fit the other multimer
    """

    with connect("membed.db") as db:
        for row_copy in db.select(calculate="F"):
            for row_paste in db.select(calculate="N"):
                if row_copy.get("mon_type") == row_paste.get("mon_type") and row_copy.get("dim_type") == None and row_copy.get("tri_type") == None:
                    if forces:
                        low_forces_orig = row_copy.data.low_forces
                        high_forces_orig = row_copy.data.high_forces
                        rotation_matrix = row_paste.data.rotation_matrix
                        low_forces_rotated = np.matmul(rotation_matrix.transpose(),low_forces_orig.transpose()).transpose()
                        high_forces_rotated = np.matmul(rotation_matrix.transpose(),high_forces_orig.transpose()).transpose()
                        db.update(row_paste.id,
                                  low_energy=row_copy.low_energy,
                                  data={"low_forces": low_forces_rotated,
                                        "high_forces": high_forces_rotated},
                                  high_energy=row_copy.high_energy)
                    else:
                        db.update(row_paste.id, low_energy=row_copy.low_energy, high_energy=row_copy.high_energy)

                elif row_copy.get("dim_type") == row_paste.get("dim_type") and row_copy.get("mon_type") == None and row_copy.get("tri_type") == None:
                    if forces:
                        low_forces_orig = row_copy.data.low_forces
                        high_forces_orig = row_copy.data.high_forces
                        rotation_matrix = row_paste.data.rotation_matrix
                        low_forces_rotated = np.matmul(rotation_matrix.transpose(),low_forces_orig.transpose()).transpose()
                        high_forces_rotated = np.matmul(rotation_matrix.transpose(),high_forces_orig.transpose()).transpose()
                        db.update(row_paste.id,
                                  low_energy=row_copy.low_energy,
                                  data={"low_forces": low_forces_rotated,
                                        "high_forces": high_forces_rotated},
                                  high_energy=row_copy.high_energy)
                    else:
                        db.update(row_paste.id, low_energy=row_copy.low_energy, high_energy=row_copy.high_energy)

                elif row_copy.get("tri_type") == row_paste.get("tri_type") and row_copy.get("mon_type") == None and row_copy.get("dim_type") == None:
                    if forces:
                        low_forces_orig = row_copy.data.low_forces
                        high_forces_orig = row_copy.data.high_forces
                        rotation_matrix = row_paste.data.rotation_matrix
                        low_forces_rotated = np.matmul(rotation_matrix.transpose(),low_forces_orig.transpose()).transpose()
                        high_forces_rotated = np.matmul(rotation_matrix.transpose(),high_forces_orig.transpose()).transpose()
                        db.update(row_paste.id,
                                  low_energy=row_copy.low_energy,
                                  data={"low_forces": low_forces_rotated,
                                        "high_forces": high_forces_rotated},
                                  high_energy=row_copy.high_energy)
                    else:
                        db.update(row_paste.id, low_energy=row_copy.low_energy, high_energy=row_copy.high_energy)


def save_displacements_to_database(supercells=None, monomers=None, dimers=None):
    """Save the displaced supercells, displaced monomers, or displaced dimers to the membed.db database"""

    if not supercells == None:
        with connect("membed.db") as db:
            for i in range(len(supercells)):
                db.write(supercells[i],
                        name="displacement%s" % str(i+1),
                        displacement_num=i+1,
                        displaced_atom=supercells[i].displacement[0],
                        data={"displacement": supercells[i].displacement})

    if not monomers == None:
        with connect("membed.db") as db:
            for i in range(len(monomers)):
                db.write(monomers[i],
                        name="disp_mon%s" % str(i+1),
                        disp_mon_num=i+1,
                        orig_monomer=monomers[i].orig_monomer,
                        disp_id=monomers[i].disp_id,
                        displaced_atom=monomers[i].displaced_atom,
                        calculate="T",
                        data={"indices": monomers[i].indices, "displacement_magnitude": monomers[i].displacement_magnitude})

    if not dimers == None:
        with connect("membed.db") as db:
            for i in range(len(dimers)):
                db.write(dimers[i],
                        name="disp_dim%s" % str(i+1),
                        disp_dim_num=i+1,
                        orig_dimer=dimers[i].orig_dimer,
                        orig_mon_a=dimers[i].orig_mon_a,
                        orig_mon_b=dimers[i].orig_mon_b,
                        disp_id=dimers[i].disp_id,
                        displaced_atom=dimers[i].displaced_atom,
                        calculate="T",
                        data={"indices": dimers[i].indices, "displacement_magnitude": dimers[i].displacement_magnitude, "mon_b_sc_sector": dimers[i].mon_b_sc_sector})





